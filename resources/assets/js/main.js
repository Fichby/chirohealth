document.addEventListener('DOMContentLoaded', function () {

  // Get all "navbar-burger" elements
  var menu = document.querySelector('.navbar-burger');

      menu.addEventListener('click', function () {

        var $target = document.querySelector('.navbar-menu');
        $target.classList.toggle('is-active');

      });

      var menu = document.querySelectorAll('.navbar-link');

        for (i = 0; i < menu.length; ++i) {
            menu[i].addEventListener('click', function () {
                for (i = 0; i < menu.length; ++i) {
                    menu[i].classList.remove('display-me');
                }
                this.nextSibling.nextSibling.classList.toggle('display-me')
            });
        }

        var searchinput = document.querySelector('#SearchInput');
        searchinput.addEventListener('keyup', function(event) {
            myFunction();
        });
        // var ths = document.querySelectorAll('th');
        // for (i = 0; i < ths.length; ++i) {
        //     ths[i].addEventListener('click', function () {
        //         sortTable(this.cellIndex)
        //     });
        // }

        function myFunction() {
          // Declare variables
          var input, filter, table, tr, td, i;
          input = document.getElementById("SearchInput");
          filter = input.value.toUpperCase();
          table = document.querySelector("table");
          tr = table.getElementsByTagName("tr");

          // Loop through all table rows, and hide those who don't match the search query
          for (i = 0; i < tr.length; i++) {
            tds = tr[i].getElementsByTagName("td");
            td1 = tr[i].getElementsByTagName("td")[1];
            for (j = 0; j < tds.length; j++){
                if (tds[j]) {
                  if (tds[j].innerHTML.toUpperCase().indexOf(filter) > -1 ) {
                    tr[i].style.display = "";
                    break;
                  } else {
                    tr[i].style.display = "none";

                  }
                }
            }

          }
        }

        const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;

        const comparer = (idx, asc) => (a, b) => ((v1, v2) =>
            v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
            )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));

        // do the work...
        document.querySelectorAll('th').forEach(th => th.addEventListener('click', (() => {
            const table = th.parentNode.parentNode.nextElementSibling;
            Array.from(table.querySelectorAll('tr:nth-child(n+1)'))
                .sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
                .forEach(tr => table.appendChild(tr) );
        })));
        $('#transactionstable').DataTable({
        "order": [[ 1, "desc" ]]
    } );





});
$(document).on('click', '.notification > button.delete', function() {
    $(this).parent().addClass('is-hidden');
    return false;
});
$('#contactUs').click(function(){
    var data = $('#contactform').serialize();
    axios.post('contactform', data)
      .then(function (response) {
        $('.forminput').empty();

      })
      .catch(function (error) {
        alert(error);
      })
      .then(function () {
        // always executed
      });
});
