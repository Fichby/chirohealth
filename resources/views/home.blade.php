@extends('layouts.app')

@section('content')

<article class="message">
  <div class="message-header">
    <p>Message</p>
    <button class="delete" aria-label="delete"></button>
  </div>
  <div class="message-body">
      @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
      @endif

      You are logged in!
  </div>
</article>
@endsection
