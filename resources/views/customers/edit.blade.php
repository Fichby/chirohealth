@extends('layouts.app')

@section('title', '| Edit User')

@section('content')

    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('usersedit', $user) }}
    </nav>
    @include('layouts.errors')

    <div class='section'>
        <h1><i class='fa fa-key'></i> Edit User: {{$user->name}}</h1>
        <hr>

    {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

    <div class="field">
        {{ Form::label('name', 'Name', array('class' => 'label')) }}
        <div class="control">
            {{ Form::text('name', null, array('class' => 'input')) }}
        </div>
    </div>

    <div class="field">
        {{ Form::label('email', 'Email', array('class' => 'label')) }}
        <div class="control">
            {{ Form::email('email', null, array('class' => 'input')) }}
        </div>
    </div>


    {{ Form::submit('Add', array('class' => 'button is-primary')) }}

    {{ Form::close() }}

</div>

@endsection
