@extends('layouts.app')

@section('title', '| Users')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('users') }}
    </nav>
    @include('layouts.errors')
<style media="screen">
    table{
        font-size:75%;
    }
</style>
        <div class="section">
            <input type="text" id="SearchInput" placeholder="Search for names.." class="input">
            <table class="table is-striped is-hoverable is-fullwidth">

            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>ID number</th>
                    <th>Address</th>
                    <th>Postal Code</th>
                    <th>Phone Home</th>
                    <th>Phone work</th>
                    <th>Phone Cell</th>
                    <th>Email</th>
                    <th>Reference</th>
                    <th>Date/Time Created</th>
                
                </tr>
            </thead>

            <tbody>
                @foreach ($users as $user)
                <tr>

                    <td>{{ $user->firstname }}</td>
                    <td>{{ $user->lastname }}</td>
                    <td>{{ $user->client_id }}</td>
                    <td>{{ $user->address}}</td>
                    <td>{{ $user->postalcode}}</td>
                    <td>{{ $user->phone_home}}</td>
                    <td>{{ $user->phone_work}}</td>
                    <td>{{ $user->phone_cell}}</td>
                    <td>{{ $user->email}}</td>

                    <td> @if(isset($user->reference->description)) {{$user->reference->description}} @endif</td>

                    <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>

                </tr>
                @endforeach
            </tbody>

        </table>

    <a href="{{ route('users.create') }}" class="button is-success">Add User</a>

</div>

@endsection
