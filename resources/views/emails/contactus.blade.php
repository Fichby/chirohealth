@component('mail::message')
# Contact Us

@component('mail::panel')
{{$name}}<br>
{{$email}} <br>
{{$comment}}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
