@component('mail::message')
# Introduction

Dear HCP
There are orders for you to approve!

@component('mail::button', ['url' => URL::to("/approve")])
View Approvals
@endcomponent

@component('mail::panel')
{{ $transaction->invdate}}<br>
{{ $transaction->invnumber}}<br>
{{ $transaction->invtotal}}
@endcomponent

@component('mail::table')
| Item       | Description         | Quantity  |
| ------------- |:-------------:| --------:|
@foreach ($transaction->details as $value)
| {{ $value->items->first()->suppl_id}}      | {{ $value->items->first()->description}}      | {{ $value->qty}}      |
@endforeach

@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
