@extends('layouts.app')

@section('title', '| Add Role')

@section('content')

    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('rolescreate') }}
    </nav>
    @include('layouts.errors')

    <div class='section'>
        <h1><i class='fa fa-key'></i> Add Role</h1>
        <hr>

    {{ Form::open(array('route' => 'roles.store')) }}

    <div class="field">
        {{ Form::label('name', 'Name', array('class' => 'label')) }}
        <div class="control">
            {{ Form::text('name', null, array('class' => 'input')) }}
        </div>
    </div>

    <h5><b>Assign Permissions</b></h5>

    <div class='field'>
        @foreach ($permissions as $permission)
            {{ Form::checkbox('permissions[]',  $permission->id ) }}
            {{ Form::label($permission->name, ucfirst($permission->name), array('class' => 'checkbox')) }}<br>

        @endforeach
    </div>

    {{ Form::submit('Add', array('class' => 'button is-primary')) }}

    {{ Form::close() }}

</div>

@endsection
