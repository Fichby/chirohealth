@extends('layouts.app')

@section('title', '| Roles')

@section('content')
<nav class="breadcrumb" aria-label="breadcrumbs">
    {{ Breadcrumbs::render('roles') }}
</nav>
@include('layouts.errors')

    <div class="section">
        <input type="text" id="SearchInput" placeholder="Search for names.." class="input">
        <table class="table is-striped is-hoverable is-fullwidth">
            <thead>
                <tr>
                    <th>Role</th>
                    <th>Permissions</th>
                    <th>Operation</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($roles as $role)
                <tr>

                    <td>{{ $role->name }}</td>

                    <td>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')) }}</td>{{-- Retrieve array of permissions associated to a role and convert to string --}}
                    <td>
                    <a href="{{ URL::to('roles/'.$role->id.'/edit') }}" class="button is-info is-pulled-left" style="margin-right: 3px;">Edit</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'button is-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>


    <a href="{{ URL::to('roles/create') }}" class="button is-success">Add Role</a>

</div>

@endsection
