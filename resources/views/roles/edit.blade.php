@extends('layouts.app')

@section('title', '| Edit Role')

@section('content')
<nav class="breadcrumb" aria-label="breadcrumbs">
    {{ Breadcrumbs::render('rolesedit', $role) }}
</nav>
@include('layouts.errors')

<div class='section'>
    <h1><i class='fa fa-key'></i> Edit Role: {{$role->name}}</h1>
    <hr>

    {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

    <div class="field">
        {{ Form::label('name', 'Role Name', array('class' => 'label')) }}
        <div class="control">
            {{ Form::text('name', null, array('class' => 'input')) }}
        </div>

    </div>

    <h5><b>Assign Permissions</b></h5>
    <div class="field">
    @foreach ($permissions as $permission)

            <div class="control">
                {{Form::checkbox('permissions[]',  $permission->id, $role->permissions ) }}
                {{Form::label($permission->name, ucfirst($permission->name), array('class' => 'checkbox')) }}<br>
            </div>


    @endforeach
    </div>
    {{ Form::submit('Edit', array('class' => 'button is-primary')) }}

    {{ Form::close() }}
</div>

@endsection
