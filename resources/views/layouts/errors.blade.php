@if(Session::has('flash_message'))
<div class="notification is-info">
    <button class="delete"></button>
    <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
</div>
@endif
@if(isset($errors))
@if (count($errors) > 0)
<div class="notification is-info">
    <button class="delete"></button>
    @include ('errors.list') {{-- Including error file --}}
</div>
@endif
@endif
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(event) {
    let notify = document.querySelector('.notification');
    let deleteme = document.querySelector('.notification>.delete');
    if (notify) {
        notify.onclick = function(){
            notify.style.display = 'none'
        };
    }

});
</script>
