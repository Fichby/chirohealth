
    <div class="column is-2">
        <aside class="menu">
            <p class="menu-label">
                General
            </p>
            <ul class="menu-list">
                <li><a class="menu-item">Dashboard</a></li>
                <li><a class="menu-item">Customers</a></li>
            </ul>
            <p class="menu-label">
                Administration
            </p>
            <ul class="menu-list">
                <li><a class="menu-item">Team Settings</a></li>
                <li>
                    <a class="menu-item">Manage Your Team</a>
                    <ul>
                        <li><a class="menu-item">Members</a></li>
                        <li><a class="menu-item">Plugins</a></li>
                        <li><a class="menu-item">Add a member</a></li>
                    </ul>
                </li>
                <li><a class="menu-item">Invitations</a></li>
                <li><a class="menu-item">Cloud Storage Environment Settings</a></li>
                <li><a class="menu-item">Authentication</a></li>
            </ul>
            <p class="menu-label">
                Auth
            </p>
            <ul class="menu-list">
                <li><a href="{{ route('roles.index') }}" class="menu-item">Roles</a></li>
                <li><a href="{{ route('permissions.index') }}" class="menu-item">Permissions</a></li>
                <li><a href="{{ route('admins.index') }}" class="menu-item">Users</a></li>
            </ul>
        </aside>
    </div>
