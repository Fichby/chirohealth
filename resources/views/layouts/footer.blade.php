<div class="modal">
  <div class="modal-background"></div>
  <div class="modal-content">
      <div class="box">
        <article class="media">
          <div class="media-content">
            <div class="content">
              <p>
                <strong>Attention</strong>
                <br>
                Please Login or Register, before Shopping.
              </p>
            </div>
            <nav class="level is-mobile">
              <div class="level-left">
                <a class="level-item" aria-label="reply">
                  <span class="icon is-small">
                    <i class="fa fa-cross" aria-hidden="true"></i>
                  </span>
                </a>
              </div>
            </nav>
          </div>
        </article>
      </div>
  </div>
  <button class="modal-close is-large" aria-label="close"></button>
</div>


<script src="/js/jquery.js"></script>
<script async type="text/javascript" src="/js/bulma.js"></script>
<script src="/js/siema.js"></script>
<script src="/js/app.js"></script>
<script defer src="/js/zscroll.js"> </script>

<script src="/js/all.js">

</script>
</body>
</html>
