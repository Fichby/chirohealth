<div class="column is-12">
    <ul class="steps">
      <li class="steps-segment is-active">
        <span class="steps-marker"></span>
        <div class="steps-content is-divider-content">
          <p class="is-size-4">Check Cart</p>
        </div>
      </li>
      <li class="steps-segment ">
        <span class="steps-marker"></span>
        <div class="steps-content is-divider-content">
          <p class="is-size-4">Delivery Info</p>
        </div>
      </li>
      <li class="steps-segment ">
        <span class="steps-marker"></span>
        <div class="steps-content is-divider-content">
          <p class="is-size-4">Confirm</p>
        </div>
      </li>

      <li class="steps-segment">
        <span class="steps-marker"></span>
      </li>
    </ul>

</div>
