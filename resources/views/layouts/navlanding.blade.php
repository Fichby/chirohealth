<nav class="navbar is-white topNav">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item" href="/">
                <h1>C-Health </h1>
                <sub>Chiropratic Health</sub>
            </a>
            <div class="navbar-burger burger" data-target="topNav">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div id="topNav" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="{{route('aboutus')}}">What We Do.</a>
                <a class="navbar-item" href="/#contact">Get In Touch</a>
                <a class="navbar-item" id="online-store" href="{{url('/store')}}">Our Online Store</a>

            </div>
            <div class="navbar-end">
                <div class="navbar-item">
                    <div class="field is-grouped">
                        <p class="control">
                            <a class="button is-small" @if (Auth::check()) href="{{route('cart.index')}}" @endif>
                                <span>
                                    Checkout
                                </span>
                                <span class="icon">
                                    <i class="fa fa-shopping-cart"></i>
                                </span>
                                <span class="cartitemscount">
                                    @if (!Auth::check())
                                        0
                                    @else
                                        {{$cartitems}}
                                    @endif
                                </span>
                            </a>
                        </p>
                        @if (!Auth::check())
                            <p class="control">
                                <a class="button is-small" href="{{route('register')}}" id="registerlink">
                                    <span class="icon">
                                        <i class="fa fa-user-plus"></i>
                                    </span>
                                    <span>
                                        Register
                                    </span>
                                </a>
                            </p>
                            <p class="control">
                                <a class="button is-small is-info is-outlined" href="{{route('login')}}" id="loginlink">
                                    <span class="icon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <span>Login</span>
                                </a>
                            </p>
                        @else
                            <p class="control">
                                <a class="button is-small is-info is-outlined" href="{{route('profile.index')}}">
                                    <span class="icon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <span>Profile</span>
                                </a>
                            </p>
                            <p class="control">Welcome: {{auth()->user()->firstname}}</p>
                            <p class="control">

                              <a class="button is-small" href="{{route('logout')}}" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();" id="logoutlink">
                                <span class="icon">
                                  <i class="fa fa-sign-out"></i>
                                </span>
                                <span>Logout</span>

                              </a>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                              </form>
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
