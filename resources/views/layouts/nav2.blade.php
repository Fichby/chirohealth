<nav class="navbar ">
  <div class="navbar-brand">
    <a class="navbar-item">
      <img src="/img/logotest.png" alt="Bulma: a modern CSS framework based on Flexbox" width="100px" >
    </a>

    <div class="navbar-burger burger" data-target="navMenubd-example">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>

  <div id="navMenubd-example" class="navbar-menu">
    <div class="navbar-start">


      <div class="navbar-item has-dropdown ">
        <a class="navbar-link" >
          User Settings
        </a>
        <div class="navbar-dropdown is-hoverable">
          <a class="navbar-item " href="{{route('admins.index')}}">
            Users
          </a>
          <a class="navbar-item " href="{{route('users.index')}}">
            Customers
          </a>
          @if(auth()->guard('admin')->user()->can('Administer roles & permissions'))
          <a class="navbar-item " href="{{route('roles.index')}}">
            Roles
          </a>
          <a class="navbar-item " href="{{route('permissions.index')}}">
            Permissions
          </a>
          @endif

        </div>
      </div>

@if(auth()->guard('admin')->user()->can('View Reports'))
      <div class="navbar-item has-dropdown ">
        <a class="navbar-link" >
          MIS Reporting
        </a>
        <div class="navbar-dropdown is-hoverable">
          <a class="navbar-item " href="{{url('patientsfortheday')}}">
            Patients for the day
          </a>
          <a class="navbar-item " href="{{url('upcomingpatientbirthdays')}}">
            Patient Birthdays
          </a>
          <a class="navbar-item " href="{{url('reorderlevels')}}">
            Stock Reorder Levels
          </a>
          <a class="navbar-item " href="{{url('patientvisits')}}">
            Patient stats Month/Year
          </a>
          <a class="navbar-item " href="{{route('supplementstats.index')}}">
            Supplement Sales Month/Year
          </a>
          <a class="navbar-item " href="{{route('top10supplements.index')}}">
            Top 10 Supplement Sales Month/Year
          </a>

        </div>
      </div>
  @endif
  @if(auth()->guard('admin')->user()->can('Administer Stock'))

      <div class="navbar-item has-dropdown ">
        <a class="navbar-link" >
          Inventory Control
        </a>
        <div class="navbar-dropdown is-hoverable">
          <a class="navbar-item " href="{{route('items.index')}}">
            Stock List
          </a>

        </div>
      </div>
  @endif
      <div class="navbar-item has-dropdown ">
        <a class="navbar-link" >
          Store
        </a>
        <div class="navbar-dropdown is-hoverable">
          <a class="navbar-item " href="{{route('transactions.index')}}">
            Transactions
          </a>
          @if(auth()->guard('admin')->user()->can('Approve Orders'))
          <a class="navbar-item " href="{{route('approve.index')}}">
            ToApprove
          </a>
            @endif
          <a class="navbar-item " href="{{route('shipments.index')}}">
            Shipments
          </a>
        </div>
      </div>
      @if(auth()->guard('admin')->user()->can('Import Data'))
      <div class="navbar-item has-dropdown ">
        <a class="navbar-link" >
          Data management
        </a>
        <div class="navbar-dropdown is-hoverable">
          <a class="navbar-item " href="{{route('import')}}">
            Import
          </a>
        </div>
      </div>
  @endif
    </div>
    <div class="navbar-end">
      <div class="navbar-item">
        <div class="field is-grouped">
          <p class="control">

            <a class="button is-primary" href="{{route('logout')}}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
              <span class="icon">
                <i class="fa fa-sign-out-alt"></i>
              </span>
              <span>Logout</span>

            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </p>
        </div>
      </div>
    </div>
  </div>
</nav>
