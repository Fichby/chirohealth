@extends('layouts.app')

@section('title', '| Items')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('itemsedit', $item) }}
    </nav>
    @include('layouts.errors')

    <div class='section'>
        <h1><i class='fafa-key'></i> Edit Item: {{$item->suppl_id}} <b class="is-pulled-right">QOH:({{$item->qtyonhand}})</b></h1>
        <hr>

        {{ Form::model($item, array('route' => array('items.update', $item->id), 'method' => 'PUT')) }}
        <div class="field is-horizontal">
          <div class="field-body">
              <div class="field">
                  {{ Form::label('name', 'Item Code', array('class' => 'label')) }}
                  <div class="control">
                      {{ Form::text('suppl_id', null, array('class' => 'input')) }}
                  </div>

              </div>
              <div class="field">
                  {{ Form::label('name', 'Item Description', array('class' => 'label')) }}
                  <div class="control">
                      {{ Form::text('description', null, array('class' => 'input')) }}
                  </div>

              </div>
          </div>
        </div>

        <div class="field is-horizontal">
            <div class="field-body">
                <div class="field">
                    {{ Form::label('name', 'Cost Exclusive', array('class' => 'label')) }}
                    <div class="control">
                        {{ Form::text('cost_excl', null, array('class' => 'input')) }}
                    </div>

                </div>
                <div class="field">
                    {{ Form::label('name', 'Cost Inclusive', array('class' => 'label')) }}
                    <div class="control">
                        {{ Form::text('cost_incl', null, array('class' => 'input')) }}
                    </div>

                </div>
            </div>
        </div>
        <div class="field is-horizontal">
            <div class="field-body">
                <div class="field">
                    {{ Form::label('name', 'Percentage Increase', array('class' => 'label')) }}
                    <div class="control">

                        @if(auth()->user()->can('Edit Percentage Markup'))
                            {{ Form::text('perc_inc', null, array('class' => 'input')) }}
                        @else
                            {{ Form::text('perc_inc', null, array('class' => 'input', 'readonly' => 'true')) }}
                        @endif
                    </div>

                </div>
                <div class="field">
                    {{ Form::label('name', 'Minimum Level', array('class' => 'label')) }}
                    <div class="control">
                        {{ Form::text('min_levels', null, array('class' => 'input')) }}
                    </div>

                </div>
            </div>
        </div>


        {{ Form::submit('Edit', array('class' => 'button is-primary is-pulled-right')) }}

        {{ Form::close() }}
    </div>
@endsection
