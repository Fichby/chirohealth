@extends('layouts.app')

@section('title', '| Items')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('items') }}
    </nav>
    @include('layouts.errors')
    <a href="{{ route('items.create') }}" class="button is-success is-pulled-right">Add Item</a>
        <div class="section">
            <h3>Reports</h3>

            <input type="text" id="SearchInput" placeholder="Search for names.." class="input">
            <table class="table is-striped is-hoverable is-fullwidth" id="searchtable">
                <thead>
                    <th>ID</th>
                    <th>Description</th>
                    <th>Cost Excl</th>
                    <th>Cost Incl</th>
                    <th>Min Level</th>
                    <th>Qty On Hand</th>
                    <th>Qty On Sales Order</th>
                    <th>Net Qty</th>
                    <th>Vendor ID</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach ($items as $item)
                        <tr>
                            <td>{{$item->suppl_id}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->cost_excl}}</td>
                            <td>{{$item->cost_incl}}</td>
                            <td>{{$item->min_levels}}</td>
                            <td>{{$item->qtyonhand}}</td>
                            <td>{{$item->qtyonsalesorder}}</td>
                            <td>{{$item->qtyonhand -$item->qtyonsalesorder}}</td>
                            <td>{{$item->vendor_id}}</td>
                            <td>
                                <a href="{{ route('items.edit', $item->id) }}" class="button is-info is-pulled-left" style="margin-right: 3px;">Edit</a>
                                @if(auth()->user()->can('Delete Items'))
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['items.destroy', $item->id] ]) !!}
                                    {!! Form::submit('Delete', ['class' => 'button is-danger']) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
@endsection
