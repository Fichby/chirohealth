@extends('layouts.app')

@section('title', '| Shipments')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('shipments') }}
    </nav>
    @include('layouts.errors')
        <div class="section">
            <h3>Shipments</h3>

            <input type="text" id="SearchInput" placeholder="Search for names.." class="input">
            <table class="table is-striped is-hoverable is-fullwidth" id="searchtable">
                <thead>
                    <th>Invnumber</th>
                    <th>Date</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach ($shipments as $item)

                        <tr>
                            <td>{{$item->transaction->invnumber}}</td>
                            <td>{{$item->transaction->invdate}}</td>

                            <td>
                                <a href="{{ route('shipments.show', $item->transaction_id) }}" class="button is-info is-pulled-left" style="margin-right: 3px;">Details</a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
@endsection
