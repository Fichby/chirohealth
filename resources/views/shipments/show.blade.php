@extends('layouts.app')

@section('title', '| Shipments')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('shipments') }}
    </nav>
    @include('layouts.errors')
        <div class="section">
            <h3>Shipments</h3>

            <input type="text" id="SearchInput" placeholder="Search for names.." class="input">
            <a href="{{ route('shipments.edit', $transaction->id) }}" class="button is-info is-pulled-right" style="margin-right: 3px;">Ship</a>
            <table class="table is-striped is-hoverable is-fullwidth" id="searchtable">
                <thead>
                    <th>Invnumber</th>
                    <th>Item</th>
                    <th>Descriptiton</th>
                    <th>Qty</th>

                </thead>
                <tbody>
                    @foreach ($transactiondetails as $item)

                        <tr>
                            <td>{{$item->transaction->invnumber}}</td>
                            <td>{{$item->items->first()->suppl_id}}</td>
                            <td>{{$item->items->first()->description}}</td>
                            <td>{{$item->qty}}</td>

                        </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
@endsection
