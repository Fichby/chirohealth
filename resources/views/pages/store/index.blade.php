@extends('layouts.landing')

@section('title', '| Shop')

@section('content')
    <style media="screen">
        .card{
            font-size:12px;
        }
        .itemdescription{
            width:60%;
        }
    </style>
<section class="container">


    <div class="columns">
        <div class="column is-2">
            <aside class="menu">

              <p class="menu-label">
                Store
              </p>
              <ul class="menu-list">

                <li>
                  <a href="{{url('/store?queryval=all')}}">Supplements</a>
                  <ul>
                    <li><a href="{{url('/store?queryval=1')}}">Pre-Workout</a></li>
                    <li><a href="{{url('/store?queryval=2')}}">Whey Protein</a></li>
                    <li><a href="{{url('/store?queryval=3')}}">Meal Replacements</a></li>
                  </ul>
                </li>

              </ul>

            </aside>
        </div>
        <div class="column is-10">
            <div class="columns is-multiline">

                @foreach ($items as $item)
                    <div class="column is-3">
                        <div class="card">
                            <div class="card-image">
                              <figure class="image is-128x128" style="margin:auto; ">
                                <img src="img/supplement.png" alt="Placeholder image" class="image-image">
                                <div class="centered myid">{{$item->id}}</div>
                              </figure>
                            </div>
                            <div class="card-content is-clearfix">
                                <div class="content">
                                        <div class="is-pulled-left itemdescription" > <span style="font-size:15px">{{$item->suppl_id}} <br> <sub>{{$item->description}}</sub> <br> <span style="fint-size:20px; color:red; float:left">@if ($item->qtyonhand > $item->min_levels) In Stock @else No Stock @endif</span></span> </div>
                                        <div class="is-pulled-right"><b>R{{number_format($item->cost_incl,2,".",",")}}</b></div>
                                        <br>
                                        <div class="is-pulled-right addcart" >
                                            <a class="button is-dark is-small level-item @if(Auth::check()) @if($item->qtyonhand > $item->min_levels) addtocart @else NoStock @endif @else pleaselogin @endif button button-info" aria-label="reply">
                                                Add
                                              {{-- <span class="icon is-medium">

                                                <i class="fa fa-plus @if(Auth::check()) addtocart @else pleaselogin @endif" aria-hidden="true" style="font-size:200%"></i>
                                              </span> --}}
                                            </a>

                                        </div>
                                </div>


                            </div>

                        </div>
                    </div>

                @endforeach
                <div class="notification is-info is-text-centered is-clearfix" id='alrt' style="padding:unset; position:fixed; top:45vh;left:45vw; "></div>
            </div>
        </div>

    </div>

</section>


@endsection
