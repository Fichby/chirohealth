@extends('layouts.landing')

@section('title', '| Shop')

@section('content')
<section class="container">
    <div class="columns">
        <div class="column is-12">
                <div class="siema">
                  <div><img src="https://source.unsplash.com/random/1600x400?cycling" alt="Siema image" /></div>
                  <div><img src="https://source.unsplash.com/random/1600x400?nutrition" alt="Siema image" /></div>
                  <div><img src="https://source.unsplash.com/random/1600x400?health" alt="Siema image" /></div>
                  <div><img src="https://source.unsplash.com/random/1600x400?sports" alt="Siema image" /></div>
                </div>
        </div>
    </div>
    <div class="title-widget">
        <span class="spanbefore"><hr></span>
        <span style="font-size: 25px">Latest Products</span>
        <span class="spanafter"><hr></span>
    </div>


    <div class="columns">
        <div class="column is-12">
            <div class="columns is-multiline">

                @foreach ($items as $item)
                    <div class="column is-3">
                        <div class="card">
                            <div class="card-image">
                              <figure class="image is-128x128" style="margin:auto; ">
                                <img src="img/supplement.png" alt="Placeholder image" class="image-image">
                                <div class="centered myid">{{$item->id}}</div>
                              </figure>
                            </div>
                            <div class="card-content">
                                <div class="content">

                                        <div class="is-pulled-left"> <span style="font-size:15px">{{$item->suppl_id}} <br> <sub>{{$item->description}}</sub></span> <br> <span style="fint-size:20px; color:red; float:left">@if ($item->qtyonhand > $item->min_levels) In Stock @else No Stock @endif</span></div>
                                        <div class="is-pulled-right" style="padding-right:5px"><b>R{{number_format($item->cost_incl,2,".",",")}}</b></div>
                                        <br>
                                        <div class="is-pulled-right addcart" >

                                          <a class="button is-dark is-small level-item @if(Auth::check()) @if($item->qtyonhand > $item->min_levels) addtocart @else NoStock @endif @else pleaselogin @endif button button-info" aria-label="reply">
                                              Add To Cart
                                            {{-- <span class="icon is-medium">

                                              <i class="fa fa-plus @if(Auth::check()) addtocart @else pleaselogin @endif" aria-hidden="true" style="font-size:200%"></i>
                                            </span> --}}
                                          </a>
                                        </div>
                                </div>
                                <br>


                            </div>

                        </div>
                    </div>
                @endforeach
                <div class="notification is-info is-text-centered" id='alrt' style="padding:unset; position:fixed; top:45vh;left:45vw; "></div>
            </div>
        </div>

    </div>

</section>
<section class="container" id="contact">
    <div class="columns">
        <div class="column is-12">
            <h2 class="title">Contact</h2>

            <form id="contactform" >


                <div class="field is-horizontal">
                    <div class="field-body">
                        <div class="field">
                            <p class="control has-icons-left">
                                <input class="input forminput" name="name" type="text" placeholder="Name">
                                <span class="icon is-small is-left">
                                    <i class="fas fa-user"></i>
                                </span>
                            </p>
                        </div>
                        <div class="field">
                            <p class="control has-icons-left has-icons-right">
                                <input class="input forminput" name="email" type="email" placeholder="Email">
                                <span class="icon is-small is-left">
                                    <i class="fas fa-envelope"></i>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="field is-horizontal">
                    <div class="field-body">
                        <div class="field">
                            <div class="control">
                                <textarea class="textarea forminput" name="comment" placeholder="Message us"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="field is-horizontal">
                    <div class="field-body">
                        <div class="field">
                            <div class="control">
                                <button type="button" class="button is-primary" id="contactUs">
                                    Send message
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
        </div>
    </div>
</section>
@endsection
