@extends('layouts.landing')

@section('title', '| About us')

@section('content')
    <style media="screen">
        .card{
            font-size:12px;
        }
        .itemdescription{
            width:60%;
        }
    </style>
<section class="container">
    <div class="columns">
        <div class="column is-12">
            <div class="content">
                <h1>About us</h1>
                <p>Chiro health - - Quality alternative healthcare services.</p>
                <p>Chiro Health is a small business focusing on alternative health care (Chiropractic care) for people in the community</p>
                <p>
                    it is an existing business and does currently have a client base. Currently there are 2 employees the owner who is a health care practitioner (Chiropractor) and a general administrative person that also acts as a secretary. Chiro Health is a growing business and is soon going to need a better system, as well as an online presence.
                </p>
                <h3>Meet The Team</h3>
                <hr>
                <img src="{{asset('/img/teammember1.png')}}" alt=""><br>
                <sub>Dr Byron Fichardt MTCChiro, CASA</sub><br><br>
                <p>Dr Byron Fichardt is our HCP (Health Care Practioner). He received his degree from the university of South Africa, in 2005 and has been a pilar for the chiropractic community ever since. </p>
                <img src="{{asset('/img/teammember2.png')}}" alt=""><br>
                <sub>John John</sub>
                <p>John John is our GA (General Administrator). He has been with us for 10 years, and is an invaluble member of the team ever since.. </p>

            </div>
        </div>
    </div>
</section>


@endsection
