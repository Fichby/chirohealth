@extends('layouts.landing')

@section('title', '| Profile')

@section('content')
    <style media="screen">
        .card{
            font-size:12px;
        }
        .itemdescription{
            width:60%;
        }
        body {
  background: #F5F7FA;
}

.stat-val {
  font-size: 3em;
  padding-top: 20px;
  font-weight: bold;
}

.stat-key {
  font-size: 1.4em;
  font-weight: 200;
}

.section.profile-heading .column.is-2-tablet.has-text-centered + .has-text-centered {
  border-left: 1px dotted rgba(0, 0, 0, 0.2);
}

.container.profile {
  margin-top: 1%;
}

.control.is-pulled-left span.select {
  margin-right: 5px;
  border-radius: 2px;
}

.modal-card .content h1 {
  padding: 40px 10px 10px;
  border-bottom: 1px solid #dadada;
}

.container.profile .profile-options .tabs ul li.link a {
  margin-bottom: 20px;
  padding: 20px;
  background-color: #F1F1F1;
}

.is-hidden {
display: none;
}
.inputfile {
		width: 0.1px;
		height: 0.1px;
		opacity: 0;
		overflow: hidden;

		z-index: -1;
    margin-top:10px;
}
.inputfile + label {

		/* 20px */
		width: 50%;
		text-overflow: ellipsis;
		white-space: nowrap;
		cursor: pointer;
		display: inline-block;
		overflow: hidden;
		padding-left: 0.75em;
		padding-right: 0.75em;
		border: 1px solid #dbdbdb;
		height: 2.285em;
		box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.1);
		line-height: 1.5rem;
		border-radius: 3px;
		padding-top: 0.3em;

		/* 10px 20px */
}
.no-js .inputfile + label {
		display: none;
}
.inputfile:focus + label,
.inputfile.has-focus + label {
		border-color: #00d1b2;
}
.inputfile + label * {
		/* pointer-events: none; */
		/* in case of FastClick lib use */
}
.inputfile + label svg {
		width: 1em;
		height: 1em;
		line-height: 1.5rem;
		vertical-align: middle;
		margin-top: -0.25em;
		/* 4px */
		margin-right: 0.25em;
		/* 4px */
}
.inputfile-2 + label {
		border: 1px solid #dbdbdb;
}
.inputfile-2:focus + label,
.inputfile-2.has-focus + label,
.inputfile-2 + label:hover {
		border-color: #00d1b2;
}
    </style>

<section class="container">

    <div class='columns'>
  <div class='container profile'>

    <div class='section profile-heading'>
      <div class='columns is-mobile is-multiline'>
        <div class='column is-2'>
          <span class='header-icon user-profile-image'>
            <img alt='' src='{{asset('storage/profile_pictures/'.$path)}}'>
          </span>
        </div>
        <div class='column is-4-tablet is-10-mobile name'>
          <p>
            <span class='title is-bold'>{{ucfirst(auth()->user()->firstname) .' ' .ucfirst(auth()->user()->lastname)}}</span>
            <br>
            <form  action="{{url('profilepicture')}}" method="post"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="field is-grouped">
                    <br/>
                    <input type="file" name="import_file" class="inputfile inputfile-2" id="file" >
                    <label for="file"><span id="filename">Choose File...</span></label>
                    <button class="button is-primary">Import File</button>
                </div>
            </form>
            <br>
          </p>
          <p class='tagline'>
            The users profile bio would go here, of course. It could be two lines or more or whatever. We should probably limit the amount of characters to ~500 at most though.
          </p>
        </div>
        <div class='column is-2-tablet is-4-mobile has-text-centered'>
          <p class='stat-val'>{{$invoicecount}}</p>
          <p class='stat-key'>Invoices</p>
        </div>
        <div class='column is-4-tablet is-4-mobile has-text-centered'>
          <p class='stat-val'>{{\Carbon\Carbon::parse(auth()->user()->created_at)->diffForHumans()}}</p>
          <p class='stat-key'>Date Registered</p>
        </div>

      </div>
    </div>
    <div class='profile-options is-fullwidth'>
      <div class='tabs is-fullwidth is-medium'>
        <ul>
          <li class='link'>
            <a href="{{route('profile.index')}}">
              <span class='icon'>
                <i class='fa fa-list'></i>
              </span>
              <span>My Transactions</span>
            </a>
          </li>
          <li class='link is-active'>
            <a href="{{route('profile.edit', auth()->user()->id)}}">
              <span class='icon'>
                <i class='fa fa-thumbs-up'></i>
              </span>
              <span>Edit Profile</span>
            </a>
          </li>

        </ul>
      </div>
    </div>
    <div class='box' style='border-radius: 0px;'>
      <!-- Main container -->
      {{ Form::model($user, array('route' => array('profile.update', $user->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with user data --}}

      <div class="field">
          {{ Form::label('firstname', 'First Name', array('class' => 'label')) }}
          <div class="control">
              {{ Form::text('firstname', null, array('class' => 'input')) }}
          </div>
      </div>
      <div class="field">
          {{ Form::label('lastname', 'Last Name', array('class' => 'label')) }}
          <div class="control">
              {{ Form::text('lastname', null, array('class' => 'input')) }}
          </div>
      </div>
      <div class="field">
          {{ Form::label('email', 'Email', array('class' => 'label')) }}
          <div class="control">
              {{ Form::email('email', null, array('class' => 'input')) }}
          </div>
      </div>
      <div class="field">
          {{ Form::label('client_id', 'Id#', array('class' => 'label')) }}
          <div class="control">
              {{ Form::text('client_id', null, array('class' => 'input')) }}
          </div>
      </div>
      <div class="field">
          {{ Form::label('address', 'Address', array('class' => 'label')) }}
          <div class="control">
              {{ Form::text('address', null, array('class' => 'input')) }}
          </div>
      </div>
      <div class="field">
          {{ Form::label('postalcode', 'Postal Code', array('class' => 'label')) }}
          <div class="control">
              {{ Form::text('postalcode', null, array('class' => 'input')) }}
          </div>
      </div>
      <div class="field">
          {{ Form::label('phone_home', 'Home Phone', array('class' => 'label')) }}
          <div class="control">
              {{ Form::text('phone_home', null, array('class' => 'input')) }}
          </div>
      </div>
      <div class="field">
          {{ Form::label('phone_work', 'Work Phone', array('class' => 'label')) }}
          <div class="control">
              {{ Form::text('phone_work', null, array('class' => 'input')) }}
          </div>
      </div>
      <div class="field">
          {{ Form::label('phone_cell', 'Cell', array('class' => 'label')) }}
          <div class="control">
              {{ Form::text('phone_cell', null, array('class' => 'input')) }}
          </div>
      </div>

      {{ Form::submit('Update', array('class' => 'button is-primary')) }}

      {{ Form::close() }}
    </div>

  </div>
</div>


</section>
<script type="text/javascript">
var file = document.getElementById("file");
file.onchange = function(){
    if(file.files.length > 0)
    {
      document.getElementById('filename').innerHTML =file.files[0].name;
    }
};
</script>

@endsection
