@extends('layouts.landing')

@section('title', '| Cart')

@section('content')
<section class="container">

    <div class="title-widget">
        <span class="spanbefore"><hr></span>
        <span style="font-size: 25px">Your Cart</span>
        <span class="spanafter"><hr></span>
    </div>
    <div class="columns is-multiline">
        @include('layouts.steps')
        <div class="column is-12">
            {{ Form::open( ['route' => ['cart.update', $cartitem->id], 'method' => 'put']) }}
            <div class="field is-horizontal">

                  <div class="field-label is-normal">
                    <label class="label">Item: </label>
                  </div>

              <div class="field-body">
                <div class="field">
                    <div class="control">
                        {{ Form::text('item', $cartitem->items[0]->suppl_id, ['class'=> 'input','readonly']) }}
                    </div>

                </div>
                <div class="field-label is-normal">
                  <label class="label">Quantity: </label>
                </div>
                <div class="field">
                    <div class="control">
                        {{ Form::text('quantity', $cartitem->quantity, ['class'=> 'input']) }}
                    </div>

                </div>
                <div class="field">
                    <div class="control">
                        {{ Form::submit('Edit', ['class' => 'button is-primary']) }}
                    </div>
                </div>
            </div>
        </div>

            {{ Form::close() }}
        </div>
    </div>

</section>

@endsection
