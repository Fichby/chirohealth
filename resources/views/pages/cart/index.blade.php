@extends('layouts.landing')

@section('title', '| Cart')

@section('content')
<section class="container">

    <div class="title-widget">
        <span class="spanbefore"><hr></span>
        <span style="font-size: 25px">Your Cart</span>
        <span class="spanafter"><hr></span>
    </div>
    <div class="columns is-multiline">
        @include('layouts.steps')
        <div class="column is-12">

            <table class="table s-striped is-narrow is-hoverable is-fullwidth">
                <thead>
                    <th>#</th>
                    <th>Description</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach ($cartitemsall as $key => $item)
                        <tr>
                            <td>{{$key}}</td>
                            <td>{{$item->items->first()->suppl_id}}-{{$item->items->first()->description}}</td>
                            <td>{{$item->quantity}}</td>
                            <td>{{$item->items->first()->cost_incl}}</td>
                            <td>
                                <a href="{{route('cart.edit', $item->id)}}" class="button is-primary is-pulled-left">Edit</a>

                                {!! Form::open(['method' => 'DELETE', 'route' => ['cart.destroy', $item->id] ]) !!}
                                {!! Form::submit('Delete', ['class' => 'button is-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="field is-grouped is-grouped-right">
              <p class="control">
                <a class="button is-primary " href="{{url('cart/step2')}}">
                  next
                </a>
              </p>
            </div>
        </div>
    </div>

</section>

@endsection
