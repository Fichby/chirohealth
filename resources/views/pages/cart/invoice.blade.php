<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style media="screen">
footer {
			   position: fixed;
			   bottom: 0cm;
			   left: 0cm;
			   right: 0cm;
			   height: 4cm;

			   /** Extra personal styles **/

			   color: black;
			   text-align: left;

		   }
</style>
	</head>

    <div class="row">
      <div class="col-xs-n pull-left">
          <a class="navbar-item" href="/">
                <h1>C-Health </h1>
                <sub>Chiropratic Health</sub>
            </a>
			<br>
			<br>
			<h5>
  			  16 Coppledrive Avenue <br>
  			  Roodepoort <br>
  			  Gauteng <br>
  			  1724
  		  </h5>
		  <br>

  		  <h4>Client ID: {{auth()->user()->client_id}}</h4>
      </div>


      <div class="col-xs-n text-right">
          <h3>Tax Invoice # 0000{{$transaction->invnumber}}</h3>
          <p >

            <small>Email : {{$userdetails->email}}</small><br>
            <strong>Name : {{$userdetails->firstname}} {{$userdetails->lastname}}</strong>
            <br>
            <div class="content" >
                <h4 >Invoice Date: {{$transaction->invdate}}</h4>
                <h3 >Address</h3>

                <p style="line-height:0.5">{{$userdetails->address}}</p>
                <p style="line-height:0.5">{{$userdetails->postalcode}}</p>
                <p style="line-height:0.5">{{$userdetails->phone_cell}}</p>
                <hr>
            </div>

          </p>
      </div>
    </div>
                      <div class="content" >

                      </div>

                    <table class="table table-borderd" >
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Description</th>
                                <th>Quantity</th>
                                <th >Price</th>
                                <th class="text-right">Extended Price</th>
                            </tr>


                        </thead>
                        <tbody>
                            @foreach ($transaction->details as $key => $item)
                                <tr>
                                    <td>{{$key}}</td>
                                    <td>{{$item->items->first()->suppl_id}}-{{$item->items->first()->description}}</td>
                                    <td>{{$item->qty}}</td>
                                    <td>{{$item->items->first()->cost_excl}}</td>
                                    <td class="price text-right">{{$item->items->first()->cost_excl * $item->qty}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4" class="has-text-right">Vat 15%</th>
                                <th class="text-right total-vat">{{number_format(($transaction->invtotal*0.85)*0.15,2)}}</th>
                            </tr>
                            <tr>
                                <th colspan="4" class="has-text-right">Total </th>
                                <th class="text-right total">{{number_format($transaction->invtotal,2)}}</th>
                            </tr>

                        </tfoot>
                    </table>
<footer>
	<p>Make EFT payment to:<br>
	Mr Byron Millan (Chiropractic Practitioner)<br>
	Fnb <br> Account number: 456123487 <br> Cheque acc <br>
	SMS proof of payment to:  0824712929 (use ORD number as reference)</p>
</footer>
</body>
</html>
