@extends('layouts.landing')

@section('title', '| Cart')

@section('content')
<section class="container">

    <div class="title-widget">
        <span class="spanbefore"><hr></span>
        <span style="font-size: 25px">Your Cart</span>
        <span class="spanafter"><hr></span>
    </div>
    <div class="columns is-multiline">
        <div class="column is-12">
            <ul class="steps">
              <li class="steps-segment ">
                <span class="steps-marker"></span>
                <div class="steps-content is-divider-content">
                  <p class="is-size-4">Check Cart</p>
                </div>
              </li>
              <li class="steps-segment is-active">
                <span class="steps-marker"></span>
                <div class="steps-content is-divider-content">
                  <p class="is-size-4">Delivery Info</p>
                </div>
              </li>
              <li class="steps-segment ">
                <span class="steps-marker"></span>
                <div class="steps-content is-divider-content">
                  <p class="is-size-4">Confirm</p>
                </div>
              </li>

              <li class="steps-segment">
                <span class="steps-marker"></span>
              </li>
            </ul>

        </div>
        <div class="column is-12">
            <form class="form" action="{{url('cart/step3')}}" method="post">
                {{ csrf_field() }}
                <div class="field">
                  <div class="control has-icons-left has-icons-right">
                    <input class="input" type="text" name="address" placeholder="Address" value="{{$userdetails->address}}">
                    <span class="icon is-left">
                      <i class="fa fa-home"></i>
                    </span>
                    @if (isset($userdetails->address))
                        <span class="icon is-right">
                          <i class="fa fa-check" style="color:#00d1b2"></i>
                        </span>
                    @endif

                  </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left has-icons-right">
                    <input class="input" type="text" name="postalcode" placeholder="Postal Code" value="{{$userdetails->postalcode}}">

                    @if (isset($userdetails->postalcode))
                        <span class="icon is-right">
                          <i class="fa fa-check" style="color:#00d1b2"></i>
                        </span>
                    @endif
                  </div>
                </div>
                <div class="field">
                  <div class="control has-icons-left has-icons-right">
                    <input class="input" type="text" name="cell" placeholder="Cell Number" value="{{$userdetails->phone_cell}}">

                    @if (isset($userdetails->phone_cell))
                        <span class="icon is-right">
                          <i class="fa fa-check" style="color:#00d1b2"></i>
                        </span>
                    @endif
                  </div>
                </div>
                <div class="field is-grouped is-grouped-right">
                  <p class="control">
                    <a class="button is-primary submit"  >
                      next
                    </a>
                  </p>
                </div>
            </form>

        </div>
    </div>

</section>

@endsection
