@extends('layouts.landing')

@section('title', '| Cart')

@section('content')
<section class="container">

    <div class="title-widget">
        <span class="spanbefore"><hr></span>
        <span style="font-size: 25px">Your Cart</span>
        <span class="spanafter"><hr></span>
    </div>
    <div class="columns is-multiline">
        <div class="column is-12">
            <ul class="steps">
              <li class="steps-segment ">
                <span class="steps-marker"></span>
                <div class="steps-content is-divider-content">
                  <p class="is-size-4">Check Cart</p>
                </div>
              </li>
              <li class="steps-segment ">
                <span class="steps-marker"></span>
                <div class="steps-content is-divider-content">
                  <p class="is-size-4">Delivery Info</p>
                </div>
              </li>
              <li class="steps-segment is-active">
                <span class="steps-marker"></span>
                <div class="steps-content is-divider-content">
                  <p class="is-size-4">Confirm</p>
                </div>
              </li>

              <li class="steps-segment">
                <span class="steps-marker"></span>
              </li>
            </ul>

        </div>
        <div class="column is-12">
            <div class="box">
              <article class="media">

                <div class="media-content">
                  <div class="content">
                    <p>
                      <small>{{$userdetails->email}}</small><br>
                      <strong>{{$userdetails->firstname}} {{$userdetails->lastname}}</strong>
                      <br>
                      <div class="content" >
                          <h5>Address</h5>
                          <p style="line-height:0.5">{{$userdetails->address}}</p>
                          <p style="line-height:0.5">{{$userdetails->postalcode}}</p>
                          <p style="line-height:0.5">{{$userdetails->phone_cell}}</p>


                      </div>

                    </p>
                    <table class="table is-narrow  is-fullwidth">
                        <thead>
                            <th>#</th>
                            <th>Description</th>
                            <th>Quantity</th>
                            <th >Price</th>
                            <th class="has-text-right">Extended Price</th>

                        </thead>
                        <tbody>
                            @foreach ($cartitemsall as $key => $item)
                                <tr>
                                    <td>{{$key}}</td>
                                    <td>{{$item->items->first()->suppl_id}}-{{$item->items->first()->description}}</td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{$item->items->first()->cost_incl}}</td>
                                    <td class="price has-text-right">{{$item->items->first()->cost_incl * $item->quantity}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4" class="has-text-right">Vat 15%</th>
                                <th class="has-text-right total-vat"></th>
                            </tr>
                            <tr>
                                <th colspan="4" class="has-text-right">Total </th>
                                <th class="has-text-right total"></th>
                            </tr>
                        </tfoot>
                    </table>
                  </div>

                </div>
              </article>
            </div>
                <div class="field is-grouped is-grouped-right">
                  <p class="control">
                    <a class="button is-danger submit"  href="{{url('cart/step2')}}">
                      back
                    </a>
                  </p>


                  <p class="control">
                    <a class="button is-primary submit"  href="{{url('cart/step4')}}">
                      Confirm
                    </a>
                  </p>
                </div>
            </form>

        </div>
    </div>

</section>

@endsection
