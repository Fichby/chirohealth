@extends('layouts.landing')

@section('title', '| Cart')

@section('content')
<section class="container">

    <div class="title-widget">
        <span class="spanbefore"><hr></span>
        <span style="font-size: 25px">Your Cart</span>
        <span class="spanafter"><hr></span>
    </div>
    <div class="columns is-multiline">
        <div class="column is-12">
            <ul class="steps">
              <li class="steps-segment ">
                <span class="steps-marker"></span>
                <div class="steps-content is-divider-content">
                  <p class="is-size-4">Check Cart</p>
                </div>
              </li>
              <li class="steps-segment ">
                <span class="steps-marker"></span>
                <div class="steps-content is-divider-content">
                  <p class="is-size-4">Delivery Info</p>
                </div>
              </li>
              <li class="steps-segment">
                <span class="steps-marker"></span>
                <div class="steps-content is-divider-content">
                  <p class="is-size-4">Confirm</p>
                </div>
              </li>

              <li class="steps-segment  is-active">
                <span class="steps-marker"></span>
              </li>
            </ul>

        </div>
        <div class="column is-12">
            <h3>Order Has been processed</h3>
            <div class="field is-grouped is-grouped-right">
              <p class="control">
                <a href="{{url('printorder/'.$transaction->id)}}" class="button is-primary ">Download Order</a>
              </p>


              <p class="control">
                <a href="{{route('profile.index')}}" class="button is-info">Back to Profile</a>
              </p>
            </div>



        </div>
    </div>

</section>

@endsection
