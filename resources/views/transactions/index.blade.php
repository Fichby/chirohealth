@extends('layouts.app')

@section('title', '| PatientsForTheDay')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('storetransactions') }}
    </nav>
    @include('layouts.errors')

        <div class="section">
            <h3><b>Store transactions</b></h3>
            <hr>
            <input type="text" id="SearchInput" placeholder="Search for names.." class="input" style="display:none">
            <table class="table is-striped is-hoverable is-fullwidth" id="transactionstable">

            <thead>
                <tr>
                    <th>InvNumber</th>
                    <th>InvDate</th>
                    <th>Customer Email</th>
                    <th>Customer First Name</th>
                    <th>Customer Last Name</th>
                    <th>invtotal</th>
                    <th>Generate Invoice</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($transactions as $transaction)
                <tr>
                    <td><a @if(!empty($transaction->invoicepath)) href="{{url('downloadinvoice/'.$transaction->id)}}" @endif>{{ $transaction->invnumber }}</a></td>
                    <td>{{ $transaction->invdate }}</td>
                    <td>@if(isset($transaction->customer)) {{$transaction->customer->email}} @endif</td>
                    <td>@if(isset($transaction->customer)) {{$transaction->customer->firstname}} @endif</td>
                    <td>@if(isset($transaction->customer)) {{$transaction->customer->lastname}} @endif</td>
                    <td>{{ $transaction->invtotal}}</td>
                    <td>@if(empty($transaction->invoicepath))
                        <a href="{{url('printinvoice/'.$transaction->id)}}" class="button is-primary">Invoice</a>
                    @endif
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>

</div>

@endsection
