@extends('layouts.app')

@section('title', '| PatientsForTheDay')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('patientsfortheday') }}
    </nav>
    @include('layouts.errors')

        <div class="section">
            <h3><b>Patients For The Day</b></h3>
            <hr>
            <input type="text" id="SearchInput" placeholder="Search for names.." class="input">
            <table class="table is-striped is-hoverable is-fullwidth">

            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Time</th>
                    <th>Duration</th>
                    <th>Comment</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($patients as $patient)
                <tr>

                    <td>{{ $patient->firstname }}</td>
                    <td>{{ $patient->lastname }}</td>
                    <td>{{ Carbon\Carbon::parse($patient->appointment_date)->format('h:ia') }}</td>
                    <td>{{ $patient->duration }} hours</td>{{-- Retrieve array of roles associated to a user and convert to string --}}
                    <td>{{ $patient->comment }}</td>
                </tr>
                @endforeach
            </tbody>

        </table>

</div>

@endsection
