@extends('layouts.app')

@section('title', '| Reports')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('reports') }}
    </nav>
    @include('layouts.errors')

        <div class="section">
            <h3>Reports</h3>
            <ul>
                <li><a href="{{route('patientsfortheday.index')}}">Patients For the Day</a></li>
            </ul>

        </div>

@endsection
