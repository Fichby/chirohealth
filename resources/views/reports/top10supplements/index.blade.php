@extends('layouts.app')

@section('title', '| Top 10 Supplement Sales')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('top10supplements') }}
    </nav>
    @include('layouts.errors')

        <div class="section">
            <h3><b>Supplement Sales Top 10</b></h3>
            <hr>
            <input type="text" id="SearchInput" placeholder="Search for names.." class="input">
            <form class="form" action="{{route('top10supplements.store')}}" method="post">
                <div class="field is-grouped">
                    <label class="label">Month: </label>
                    <div class="control">

                        <div class="select">
                            <select  name="month">
                                <option value="" readonly >Select One...</option>
                                <option value="1" @if($month == 1 ) selected @endif>Jan</option>
                                <option value="2" @if($month == 2 ) selected @endif>Feb</option>
                                <option value="3"@if($month == 3 ) selected @endif>Mar</option>
                                <option value="4"@if($month == 4 ) selected @endif>Apr</option>
                                <option value="5"@if($month == 5 ) selected @endif>May</option>
                                <option value="6"@if($month == 6 ) selected @endif>Jun</option>
                                <option value="7"@if($month == 7 ) selected @endif>Jul</option>
                                <option value="8"@if($month == 8 ) selected @endif>Aug</option>
                                <option value="9"@if($month == 9 ) selected @endif>Sep</option>
                                <option value="10"@if($month == 10 ) selected @endif>Oct</option>
                                <option value="11"@if($month == 11 ) selected @endif>Nov</option>
                                <option value="12"@if($month == 12 ) selected @endif>Dec</option>
                            </select>
                        </div>

                    </div>
                    <label class="label">Year: </label>
                    <div class="control">

                        <div class="select">
                            <select name="year">
                                <option value="" readonly >Select One...</option>
                                <option value="2017" @if($year == 2017 ) selected @endif>2017</option>
                                <option value="2018" @if($year == 2018 ) selected @endif>2018</option>
                                <option value="2019" @if($year == 2019 ) selected @endif>2019</option>
                            </select>
                        </div>

                    </div>
                    <input type="submit" class="button is-primary" value="Submit">
                </div>

            </form>
            <table class="table is-striped is-hoverable is-fullwidth">

            <thead>
                <tr>
                    <th>Year</th>
                    <th>Month</th>
                    <th>Suppl ID</th>
                    <th>Description</th>
                    <th>Count</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($top10s as $top10)
                <tr>

                    <td>{{ $top10->year }}</td>
                    <td>{{ date("F", mktime(0, 0, 0, $top10->month , 1)) }}</td>
                    <td>{{$top10->suppl_id}}</td>
                    <td>{{ $top10->description }}</td>
                    <td>{{ $top10->cnt}}</td>
                </tr>
                @endforeach
            </tbody>

        </table>

</div>

@endsection
