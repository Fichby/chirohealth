@extends('layouts.app')

@section('title', '| ReOrderLevels')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('reorderlevels') }}
    </nav>
    @include('layouts.errors')

        <div class="section">
            <h3><b>Re-Order Levels</b></h3>
            <hr>
            <input type="text" id="SearchInput" placeholder="Search for names.." class="input">
            <table class="table is-striped is-hoverable is-fullwidth">

            <thead>
                <tr>
                    <th>ID</th>
                    <th>Description</th>
                    <th>Min_Level</th>
                    <th>QOH</th>
                    <th>Vendor</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($items as $item)
                <tr>

                    <td>{{ $item->suppl_id }}</td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $item->min_levels}}</td>
                    <td>{{ $item->qtyonhand }}</td>
                    <td>
                        <ul>
                            <li><b>Vendor:</b> {{$item->vendordesc}}</li>
                            <li><b>Phone: </b>{{$item->phone_work}}</li>
                            <li><b>Email: </b>{{$item->email}}</li>
                        </ul>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>

</div>

@endsection
