@extends('layouts.app')

@section('title', '| UpComingBirthdays')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('upcomingpatientbirthdays') }}
    </nav>
    @include('layouts.errors')

        <div class="section">
            <h3><b>Up Coming Patient Birthdays</b></h3>
            <hr>
            <input type="text" id="SearchInput" placeholder="Search for names.." class="input">
            <table class="table is-striped is-hoverable is-fullwidth">

            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Birthdate</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($patients as $patient)
                <tr>

                    <td>{{ $patient->firstname }}</td>
                    <td>{{ $patient->lastname }}</td>
                    <td>{{ $patient->email}}</td>
                    <td>{{ Carbon\Carbon::parse($patient->birthdate)->format('d M Y') }}</td>

                </tr>
                @endforeach
            </tbody>

        </table>

</div>

@endsection
