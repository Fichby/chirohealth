@extends('layouts.app')

@section('title', '| Edit Permission')

@section('content')
    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('permissionsedit', $permission) }}
    </nav>
    @include('layouts.errors')

    <div class='section'>
        <h1><i class='fa fa-key'></i> Edit Permission: {{$permission->name}}</h1>
        <hr>

        {{ Form::model($permission, array('route' => array('permissions.update', $permission->id), 'method' => 'PUT')) }}

        <div class="field">
            {{ Form::label('name', 'Permission Name', array('class' => 'label')) }}
            <div class="control">
                {{ Form::text('name', null, array('class' => 'input')) }}
            </div>

        </div>

        {{ Form::submit('Edit', array('class' => 'button is-primary')) }}

        {{ Form::close() }}
    </div>

@endsection
