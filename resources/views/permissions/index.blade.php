@extends('layouts.app')

@section('title', '| Permissions')

@section('content')

    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('permissions') }}
    </nav>
    @include('layouts.errors')

        <div class="section">
            <input type="text" id="SearchInput" placeholder="Search for names.." class="input">
            <table class="table is-striped is-hoverable is-fullwidth">

            <thead>
                <tr>
                    <th>Permissions</th>
                    <th>Operation</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permissions as $permission)
                <tr>
                    <td>{{ $permission->name }}</td>
                    <td>
                    <a href="{{ URL::to('permissions/'.$permission->id.'/edit') }}" class="button is-info is-pulled-left" style="margin-right: 3px;">Edit</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'button is-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    <a href="{{ URL::to('permissions/create') }}" class="button is-success">Add Permission</a>

</div>

@endsection
