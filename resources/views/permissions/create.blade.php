@extends('layouts.app')

@section('title', '| Create Permission')

@section('content')

    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('permissionscreate') }}
    </nav>
    @include('layouts.errors')

    <div class='section'>
        <h1><i class='fa fa-key'></i> Add Permission</h1>
        <hr>

    {{ Form::open(array('route' => 'permissions.store')) }}

    <div class="field">
        {{ Form::label('name', 'Name', array('class' => 'label')) }}
        <div class="control">
            {{ Form::text('name', '', array('class' => 'input')) }}
        </div>

    </div><br>
    @if(!$roles->isEmpty())
        <h4><b>Assign Permission to Roles</b></h4>
        <div class="field">
            @foreach ($roles as $role)
                {{ Form::checkbox('roles[]',  $role->id ) }}
                {{ Form::label($role->name, ucfirst($role->name), array('class' => 'checkbox')) }}<br>

            @endforeach
        </div>

    @endif
    <br>
    {{ Form::submit('Add', array('class' => 'button is-primary')) }}

    {{ Form::close() }}

</div>

@endsection
