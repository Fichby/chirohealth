@extends('layouts.app')

@section('title', '| Items')

@section('content')
<style media="screen">
.is-hidden {
display: none;
}
.inputfile {
		width: 0.1px;
		height: 0.1px;
		opacity: 0;
		overflow: hidden;

		z-index: -1;
    margin-top:10px;
}
.inputfile + label {

		/* 20px */
		width: 50%;
		text-overflow: ellipsis;
		white-space: nowrap;
		cursor: pointer;
		display: inline-block;
		overflow: hidden;
		padding-left: 0.75em;
		padding-right: 0.75em;
		border: 1px solid #dbdbdb;
		height: 2.285em;
		box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.1);
		line-height: 1.5rem;
		border-radius: 3px;
		padding-top: 0.3em;

		/* 10px 20px */
}
.no-js .inputfile + label {
		display: none;
}
.inputfile:focus + label,
.inputfile.has-focus + label {
		border-color: #00d1b2;
}
.inputfile + label * {
		/* pointer-events: none; */
		/* in case of FastClick lib use */
}
.inputfile + label svg {
		width: 1em;
		height: 1em;
		line-height: 1.5rem;
		vertical-align: middle;
		margin-top: -0.25em;
		/* 4px */
		margin-right: 0.25em;
		/* 4px */
}
.inputfile-2 + label {
		border: 1px solid #dbdbdb;
}
.inputfile-2:focus + label,
.inputfile-2.has-focus + label,
.inputfile-2 + label:hover {
		border-color: #00d1b2;
}
</style>
    <div class="container">

              <div class="card">
                  <div class="card-content">
                      <form  action="{{ url('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                          @csrf

                          @if ($errors->any())
                              <div class="notification is-danger">
                                  <button type="button"  class="delete"></button>
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div>
                          @endif

                          @if (Session::has('success'))

                              <div class="notification is-success">
                                <button  type="button"  class="delete"></button>
                                <p>{{ Session::get('success') }}</p>
                              </div>
                          @elseif(Session::has('failed'))
                              <div class="notification is-danger">
                                <button type="button" class="delete"></button>
                                <p>{{ Session::get('failed') }}</p>
                              </div>
                          @endif
                          <div class="field is-grouped">
                              <br/>
                              <input type="file" name="import_file" class="inputfile inputfile-2" id="file" >
                              <label for="file"><span id="filename">Choose File...</span></label>
                          <button class="button is-primary">Import File</button>
                      </div>
                      </form>
                  </div>
              </div>


          </div>
        </div>
    </div>
    <script type="text/javascript">
    var file = document.getElementById("file");
    file.onchange = function(){
        if(file.files.length > 0)
        {
          document.getElementById('filename').innerHTML =file.files[0].name;
        }
    };
    </script>
@endsection
