<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <!-- Bulma Version 0.7.1-->

    <link rel="stylesheet" type="text/css" href="../css/admin.css">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bulma.css') }}" rel="stylesheet">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
</head>
<style media="screen">
html,body {
font-family: 'Open Sans', serif;
font-size: 14px;
font-weight: 300;
}
.hero.is-success {
background: #F2F6FA;
}
.hero .nav, .hero.is-success .nav {
-webkit-box-shadow: none;
box-shadow: none;
}
.box {
margin-top: 5rem;
}
.avatar {
margin-top: -70px;
padding-bottom: 20px;
}
.avatar img {
padding: 5px;
background: #fff;
border-radius: 50%;
-webkit-box-shadow: 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
box-shadow: 0 2px 3px rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.1);
}
input {
font-weight: 300;
}
p {
font-weight: 700;
}
p.subtitle {
padding-top: 1rem;
}
</style>
<body>
    <section class="hero is-success is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-4 is-offset-4">
                    <h3 class="title has-text-grey">Register</h3>
                    <p class="subtitle has-text-grey">Please fill in the following fields</p>
                    <div class="box">

                        <form method="POST" action="{{ route('register') }}">
@csrf
                            <div class="field">
                                <div class="control">
                                    <input id="firstname" name="firstname" class="input is-large{{ $errors->has('firstname') ? ' is-invalid' : '' }}" type="text" placeholder="Your First Name" value="{{ old('name') }}" autofocus required>

                                @if ($errors->has('firstname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input id="lastname" name="lastname" class="input is-large{{ $errors->has('lastname') ? ' is-invalid' : '' }}" type="text" placeholder="Your Last Name" value="{{ old('name') }}" autofocus required>

                                @if ($errors->has('lastname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input id="client_id" name="client_id" class="input is-large{{ $errors->has('client_id') ? ' is-invalid' : '' }}" type="text" placeholder="Your ID#" value="{{ old('name') }}" autofocus required>

                                @if ($errors->has('client_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('client_id') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input id="address" name="address" class="input is-large{{ $errors->has('address') ? ' is-invalid' : '' }}" type="text" placeholder="Your Address" value="{{ old('name') }}" autofocus required>

                                @if ($errors->has('address'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input id="email" name="email" class="input is-large{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" placeholder="Your Email" value="{{ old('email') }}" autofocus required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input id="password" class="input is-large{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" type="password" placeholder="Your Password" Required>
				                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input id="password-confirm" class="input is-large" name="password_confirmation" type="password" placeholder="Retype Your Password" Required>

                                </div>
                            </div>

                            <button class="button is-block is-info is-large is-fullwidth" id="registerbutton">Login</button>
                        </form>
                    </div>
                    <p class="has-text-grey">
                        <a href="{{ route('login') }}">Already Registered</a> &nbsp;·&nbsp;

                    </p>
                </div>
            </div>
        </div>
    </section>
    <script async type="text/javascript" src="../js/bulma.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
</body>

</html>
