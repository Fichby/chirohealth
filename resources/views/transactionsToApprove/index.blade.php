@extends('layouts.app')

@section('title', '| Transaction To Approve')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('toapprove') }}
    </nav>
    @include('layouts.errors')
        <div class="section">
            <h3>Reports</h3>

            <input type="text" id="SearchInput" placeholder="Search for names.." class="input">
            <table class="table is-striped is-hoverable is-fullwidth" id="searchtable">
                <thead>
                    <th>Invnumber</th>
                    <th>ID</th>
                    <th>Description</th>
                    <th>Qty</th>

                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach ($transactions as $item)
                        <tr>
                            <td>{{$item->invnumber}}</td>
                            <td>{{$item->suppl_id}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->qty}}</td>

                            <td>
                                <a href="{{ url('approveme?tran_id='. $item->tran_id.'&item_id='.$item->item_id) }}" class="button is-info is-pulled-left" style="margin-right: 3px;">Approve</a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
@endsection
