@extends('layouts.app')

@section('title', '| Users')

@section('content')


    <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('users') }}
    </nav>
    @include('layouts.errors')

        <div class="section">
            <input type="text" id="SearchInput" placeholder="Search for names.." class="input">
            <table class="table is-striped is-hoverable is-fullwidth">

            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Date/Time Added</th>
                    <th>User Roles</th>
                    @if(auth()->guard('admin')->user()->can('Administer roles & permissions'))
                    <th>Operations</th>
                @endif
                </tr>
            </thead>

            <tbody>
                @foreach ($users as $user)
                <tr>

                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
                    <td>{{  $user->roles()->pluck('name')->implode(' ') }}</td>{{-- Retrieve array of roles associated to a user and convert to string --}}
                    @if(auth()->guard('admin')->user()->can('Administer roles & permissions'))
                    <td>
                    <a href="{{ route('admins.edit', $user->id) }}" class="button is-info is-pulled-left" style="margin-right: 3px;">Edit</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['admins.destroy', $user->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'button is-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                @endif
                </tr>
                @endforeach
            </tbody>

        </table>
@if(auth()->guard('admin')->user()->can('Administer roles & permissions'))
    <a href="{{ route('admins.create') }}" class="button is-success">Add User</a>
@endif
</div>

@endsection
