@extends('layouts.app')

@section('title', '| Add User')

@section('content')

  <nav class="breadcrumb" aria-label="breadcrumbs">
        {{ Breadcrumbs::render('userscreate') }}
    </nav>
    @include('layouts.errors')

    <div class='section'>
        <h1><i class='fa fa-key'></i> Add User</h1>
        <hr>

    {{ Form::open(array('route' => 'admins.store')) }}

    <div class="field">
        {{ Form::label('name', 'Name', array('class' => 'label'))}}
        <div class="control">
            {{ Form::text('name', '', array('class' => 'input')) }}
        </div>
    </div>

    <div class="field">
        {{ Form::label('email', 'Email', array('class' => 'label')) }}
        <div class="control">
            {{ Form::email('email', '', array('class' => 'input')) }}
        </div>
    </div>

    <h5><b>Assign Roles</b></h5>

    <div class='field'>
        @foreach ($roles as $role)
            {{ Form::checkbox('roles[]',  $role->id ) }}
            {{ Form::label($role->name, ucfirst($role->name), array('class' => 'checkbox')) }}<br>

        @endforeach
    </div>

    <div class="field">
        {{ Form::label('password', 'Password', array('class' => 'label')) }}
        <div class="control">
            {{ Form::password('password', array('class' => 'input')) }}
        </div>
    </div>

    <div class="field">
        {{ Form::label('password', 'Confirm Password', array('class' => 'label')) }}
        <div class="control">
            {{ Form::password('password_confirmation', array('class' => 'input')) }}
        </div>
    </div>

    {{ Form::submit('Add', array('class' => 'button is-primary')) }}

    {{ Form::close() }}

</div>

@endsection
