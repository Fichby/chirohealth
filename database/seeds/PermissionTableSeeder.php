<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Permission::where('guard_name','admin')->truncate();

        Permission::create(['guard_name' => 'admin','name' => 'Administer roles & permissions']);
        Permission::create(['guard_name' => 'admin','name' => 'View Reports']);
        Permission::create(['guard_name' => 'admin','name' => 'Administer Stock']);
        Permission::create(['guard_name' => 'admin','name' => 'Delete Items']);
        Permission::create(['guard_name' => 'admin','name' => 'Edit Percentage Markup']);
        Permission::create(['guard_name' => 'admin','name' => 'Import Data']);
        Permission::create(['guard_name' => 'admin','name' => 'Approve Orders']);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
