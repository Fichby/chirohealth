<?php

use Illuminate\Database\Seeder;
use App\Admin;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::truncate();
        $admin = New Admin;
        $admin->name = 'byron';
        $admin->email = 'byron@gmail.com';
        $admin->password = 'secret';
        $admin->save();
        $admin->assignRole('admin');

        $admin = New Admin;
        $admin->name = 'tiaan';
        $admin->email = 'tiaan@gmail.com';
        $admin->password = 'secret';
        $admin->save();
        $admin->assignRole('HCP');

        $admin = New Admin;
        $admin->name = 'dave';
        $admin->email = 'dave@gmail.com';
        $admin->password = 'secret';
        $admin->save();
        $admin->assignRole('GA');


    }
}
