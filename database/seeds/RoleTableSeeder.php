<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Role::where('guard_name','admin')->truncate();
        $thisrole = new Role;
        $thisrole->guard_name = 'admin';
        $thisrole->name = 'admin';
        $thisrole->save();
        $thisrole->givePermissionTo(['Administer roles & permissions','View Reports','Administer Stock','Delete Items','Edit Percentage Markup','Import Data','Approve Orders']);

        $thisrole = new Role;
        $thisrole->guard_name = 'admin';
        $thisrole->name = 'HCP';
        $thisrole->save();
        $thisrole->givePermissionTo(['View Reports','Administer Stock','Delete Items','Edit Percentage Markup','Approve Orders']);

        $thisrole = new Role;
        $thisrole->guard_name = 'admin';
        $thisrole->name = 'GA';
        $thisrole->save();
        $thisrole->givePermissionTo(['Administer Stock']);
         DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
