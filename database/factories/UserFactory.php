<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstname,
        'lastname' => $faker->lastname,
        'client_id' => $faker->numberBetween(1000000000000,9999999999999),
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'address' => $faker->streetaddress,
        'phone_cell' => $faker->e164PhoneNumber,
        'phone_work' => $faker->e164PhoneNumber,
        'phone_home' => $faker->e164PhoneNumber,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Admin::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
    ];
});

$factory->define(App\Item::class, function (Faker $faker) {
    return [
        'description' => $faker->realText(rand(10,20)),
        'suppl_id' => $faker->numberBetween(100,999),
        'cost_excl' => $faker->numberBetween(100,999),
        'cost_incl' => $faker->numberBetween(100,999),
        'perc_inc' => $faker->numberBetween(10,20), // secret
        'min_levels' => $faker->numberBetween(10,20),
        'qtyonhand' => $faker->numberBetween(10,100),
    ];
});
