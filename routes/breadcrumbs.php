<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('admin'));
});

// Home > About
Breadcrumbs::for('roles', function ($trail) {
    $trail->parent('home');
    $trail->push('Roles', route('roles.index'));
});

Breadcrumbs::for('users', function ($trail) {
    $trail->parent('home');
    $trail->push('Users', route('admins.index'));
});
Breadcrumbs::for('reports', function ($trail) {
    $trail->parent('home');
    $trail->push('Reports', route('reports.index'));
});
Breadcrumbs::for('items', function ($trail) {
    $trail->parent('home');
    $trail->push('Items', route('items.index'));
});
Breadcrumbs::for('itemsedit', function ($trail, $item) {
    $trail->parent('items');
    $trail->push($item->id, route('items.edit', $item->id));
});
Breadcrumbs::for('itemscreate', function ($trail) {
    $trail->parent('items');
    $trail->push('add', route('items.create'));
});

Breadcrumbs::for('upcomingpatientbirthdays', function ($trail) {
    $trail->parent('reports');
    $trail->push('Up Coming Patient Birthdays', url('upcomingpatientbirthdays'));
});
Breadcrumbs::for('storetransactions', function ($trail) {
    $trail->parent('home');
    $trail->push('Customer transactions', url('transactions'));
});
Breadcrumbs::for('shipments', function ($trail) {
    $trail->parent('home');
    $trail->push('shipments', url('shipments'));
});
Breadcrumbs::for('toapprove', function ($trail) {
    $trail->parent('home');
    $trail->push('Transactions to Approve', url('approve'));
});

Breadcrumbs::for('patientsfortheday', function ($trail) {
    $trail->parent('reports');
    $trail->push('Patients for the day', url('patientsfortheday'));
});
Breadcrumbs::for('patientstats', function ($trail) {
    $trail->parent('reports');
    $trail->push('Patient Stats', url('patientstats'));
});
Breadcrumbs::for('supplementstats', function ($trail) {
    $trail->parent('reports');
    $trail->push('Supplement Stats', url('supplementstats'));
});
Breadcrumbs::for('top10supplements', function ($trail) {
    $trail->parent('reports');
    $trail->push('Top 10 Sales', url('top10supplements'));
});
Breadcrumbs::for('reorderlevels', function ($trail) {
    $trail->parent('reports');
    $trail->push('Stock Re-Order Levels', url('reorderlevels'));
});

Breadcrumbs::for('permissions', function ($trail) {
    $trail->parent('home');
    $trail->push('Permissions', route('permissions.index'));
});

// Home > Blog
Breadcrumbs::for('blog', function ($trail) {
    $trail->parent('home');
    $trail->push('Blog', route('blog'));
});

// Home > Blog > [Category]
Breadcrumbs::for('category', function ($trail, $category) {
    $trail->parent('blog');
    $trail->push($category->title, route('category', $category->id));
});

// Home > Blog > [Category] > [Post]
Breadcrumbs::for('rolesedit', function ($trail, $role) {
    $trail->parent('roles');
    $trail->push($role->id, route('roles.index', $role->id));
});

Breadcrumbs::for('usersedit', function ($trail, $user) {
    $trail->parent('users');
    $trail->push($user->id, route('admins.index', $user->id));
});

Breadcrumbs::for('permissionsedit', function ($trail, $permission) {
    $trail->parent('permissions');
    $trail->push($permission->id, route('permissions.index', $permission->id));
});

Breadcrumbs::for('userscreate', function ($trail) {
    $trail->parent('users');
    $trail->push('Create', route('admins.create'));
});

Breadcrumbs::for('rolescreate', function ($trail) {
    $trail->parent('roles');
    $trail->push('Create', route('roles.create'));
});

Breadcrumbs::for('permissionscreate', function ($trail) {
    $trail->parent('permissions');
    $trail->push('Create', route('permissions.create'));
});
