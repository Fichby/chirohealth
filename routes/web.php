<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('aboutus', 'CartController@aboutus')->name('aboutus');
Route::get('/admin', 'HomeController@index')->name('admin');
Route::get('/', 'CartController@landing')->name('landing');
Route::post('/addtocart', 'CartController@store');
Route::get('/store', 'CartController@onlinestore');
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
Route::post('contactform', 'HomeController@contactform');

Route::group(['middleware' => ['auth']], function() {
    Route::get('cart/step2', 'CartController@step2');
    Route::post('cart/step3', 'CartController@step3');
    Route::get('cart/step4', 'CartController@step4');
    Route::get('cart/step5', 'CartController@step5')->name('cart.step4'); //possible null route
    Route::resource('cart', 'CartController');
    Route::get('profiletransactions', 'ProfileController@profiletransactions')->name('profile.transactions'); //possible null route
    Route::get('profiletransactiondetails', 'ProfileController@profiletransactiondetails')->name('profile.transaction.details'); //possible null route
    Route::resource('profile', 'ProfileController');
    Route::get('printorder/{id}', 'CartController@printorder');
    Route::get('downloadinvoiceuser/{id}', 'CartController@downloadinvoiceuser')->name('downloadinvoiceuser');
    Route::post('profilepicture', 'ProfileController@profilepicture');

});

Route::group(['middleware' => 'auth:admin'], function() {
    Route::get('/admin', 'HomeController@index')->name('admin');
    Route::get('printinvoice/{id}', 'CartController@printinvoice');
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('admins','AdminController');
    Route::resource('permissions', 'PermissionController');
    Route::resource('transactions', 'TransactionController');
    Route::get('patientsfortheday', 'Reports@patientsfortheday');
    Route::get('upcomingpatientbirthdays', 'Reports@upcomingpatientbirthdays');
    Route::get('reorderlevels', 'Reports@reorderlevels');
    Route::get('patientvisits', 'PatientVisits@index');
    Route::post('patientvisits', 'PatientVisits@store')->name('patientvisits.store');
    Route::get('supplementstats', 'SupplementStats@index')->name('supplementstats.index');
    Route::post('supplementstats', 'SupplementStats@store')->name('supplementstats.store');
    Route::get('top10supplements', 'Top10Supplements@index')->name('top10supplements.index');
    Route::post('top10supplements', 'Top10Supplements@store')->name('top10supplements.store');
    Route::resource('reports', 'Reports');
    Route::resource('items', 'ItemController');
    Route::get('downloadinvoice/{id}', 'TransactionController@downloadinvoice')->name('downloadinvoice');
    Route::get('import', 'MaatwebsiteController@import')->name('import');
    Route::post('importExcel', 'MaatwebsiteController@importExcel')->name('import.store');
    Route::get('approveme', 'TransactionToApproveController@store')->name('approve.store');
    Route::resource('approve', 'TransactionToApproveController');
    Route::resource('shipments', 'ShipmentsController');

});
