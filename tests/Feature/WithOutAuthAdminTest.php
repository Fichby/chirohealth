<?php

namespace Tests\Feature;

use Tests\TestCase;
// use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Admin;
use App\Item;


class WithOutAuthAdminTest extends TestCase
{
    use WithoutMiddleware;


    protected $user;


    public function setUp()
    {
       parent::setUp();
       $this->user = factory(Admin::class)->create();
       $this->actingAs($this->user, 'admin');
    }
    public function testExample()
    {

        $response = $this->get('/roles');
        $response->assertStatus(200);

        $response = $this->get('/users');
        $response->assertStatus(200);

        $response = $this->get('/admins');
        $response->assertStatus(200);

        $response = $this->get('/permissions');
        $response->assertStatus(200);


    }
}
