<?php

namespace Tests\Feature;

use Tests\TestCase;
// use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use App\Item;


class WithOutAuthTest extends TestCase
{
    use WithoutMiddleware;


    protected $user;
    protected $item;

    public function setUp()
    {
       parent::setUp();
       $this->user = factory(User::class)->create();
       $this->actingAs($this->user);
       $this->item = factory(Item::class)->create();
    }
    public function testExample()
    {

        $data = [
                    'item_id' => $this->item->id
               ];

        //$user = factory(User::class)->create();
        $response = $this->json('POST', '/addtocart', $data);
        //$response = $this->call('POST','/addtocart',$data);
        $response->assertStatus(200);

        $response = $this->get('/cart/step2');
        $response->assertStatus(200);

        $response = $this->get('/cart/step4'); //sends mailgun email
        $response->assertStatus(200);

        $response = $this->get('/cart');
        $response->assertStatus(200);

        $response = $this->get('/profile');
        //$response->dump();
        $response->assertStatus(200);


    }
}
