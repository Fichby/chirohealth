<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRegister()
    {
        $this->browse(function (Browser $browser) {
            $firstname = str_random(10);
            $lastname = str_random(10);
            $email = str_random(10).'@gmail.com';
            $client_id = rand(13, 13);
            $browser->visit('/');
            $browser->assertSee('C-Health');
            $browser->click('#registerlink');
            $browser->assertSee('Register');
            $browser->type('firstname', $firstname);
            $browser->type('lastname', $lastname);
            $browser->type('client_id', $client_id);
            $browser->type('address', $firstname);
            $browser->type('email', $email);
            $browser->type('password', 'testme');
            $browser->type('password_confirmation', 'testme');
            $browser->click('#registerbutton');
            $browser->assertSee('Welcome: '.$firstname);
            $browser->click('#logoutlink');
            $browser->assertSee('Register');
        });
    }
}
