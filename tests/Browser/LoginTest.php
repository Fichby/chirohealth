<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testLogin()
    {

        $this->browse(function (Browser $browser) {

            $browser->visit('/');
            $browser->assertSee('C-Health');
            $browser->click('#loginlink');
            $browser->assertSee('Login');
            $browser->type('email', 'test@gmail.com');
            $browser->type('password', 'testme');
            $browser->click('#loginbutton');
            $browser->assertSee('Welcome: test');
            $browser->click('#logoutlink');
            $browser->assertSee('Register');
        });
    }

}
