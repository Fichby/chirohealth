/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 30);
/******/ })
/************************************************************************/
/******/ ({

/***/ 30:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(31);


/***/ }),

/***/ 31:
/***/ (function(module, exports) {

document.addEventListener('DOMContentLoaded', function () {
  var _this = this;

  // Get all "navbar-burger" elements
  var menu = document.querySelector('.navbar-burger');

  menu.addEventListener('click', function () {

    var $target = document.querySelector('.navbar-menu');
    $target.classList.toggle('is-active');
  });

  var menu = document.querySelectorAll('.navbar-link');

  for (i = 0; i < menu.length; ++i) {
    menu[i].addEventListener('click', function () {
      for (i = 0; i < menu.length; ++i) {
        menu[i].classList.remove('display-me');
      }
      this.nextSibling.nextSibling.classList.toggle('display-me');
    });
  }

  var searchinput = document.querySelector('#SearchInput');
  searchinput.addEventListener('keyup', function (event) {
    myFunction();
  });
  // var ths = document.querySelectorAll('th');
  // for (i = 0; i < ths.length; ++i) {
  //     ths[i].addEventListener('click', function () {
  //         sortTable(this.cellIndex)
  //     });
  // }

  function myFunction() {
    // Declare variables
    var input, filter, table, tr, td, i;
    input = document.getElementById("SearchInput");
    filter = input.value.toUpperCase();
    table = document.querySelector("table");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
      tds = tr[i].getElementsByTagName("td");
      td1 = tr[i].getElementsByTagName("td")[1];
      for (j = 0; j < tds.length; j++) {
        if (tds[j]) {
          if (tds[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
            break;
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
  }

  var getCellValue = function getCellValue(tr, idx) {
    return tr.children[idx].innerText || tr.children[idx].textContent;
  };

  var comparer = function comparer(idx, asc) {
    return function (a, b) {
      return function (v1, v2) {
        return v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2);
      }(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
    };
  };

  // do the work...
  document.querySelectorAll('th').forEach(function (th) {
    return th.addEventListener('click', function () {
      var table = th.parentNode.parentNode.nextElementSibling;
      Array.from(table.querySelectorAll('tr:nth-child(n+1)')).sort(comparer(Array.from(th.parentNode.children).indexOf(th), _this.asc = !_this.asc)).forEach(function (tr) {
        return table.appendChild(tr);
      });
    });
  });
  $('#transactionstable').DataTable({
    "order": [[1, "desc"]]
  });
});
$(document).on('click', '.notification > button.delete', function () {
  $(this).parent().addClass('is-hidden');
  return false;
});
$('#contactUs').click(function () {
  var data = $('#contactform').serialize();
  axios.post('contactform', data).then(function (response) {
    $('.forminput').empty();
  }).catch(function (error) {
    alert(error);
  }).then(function () {
    // always executed
  });
});

/***/ })

/******/ });