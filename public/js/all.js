Number.prototype.formatMoney = function(c, d, t){
    var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
if ($('.siema').length) {
    const mySiema = new Siema({
        selector: '.siema',
        easing: 'ease-out',
        duration: 10,
        loop: true,
    });

    setInterval(() => mySiema.next(), 5000);
}

$('.rating input').change(function () {
  var $radio = $(this);
  $('.rating .selected').removeClass('selected');
  $radio.closest('label').addClass('selected');
});
$('.pleaselogin').each(function(){
    $(this).click(function(){
        $('.modal').toggleClass('is-active');
    });
});
$('.NoStock').each(function(){
    $(this).click(function(){
        alert('There is not enough stock to do that!');

    });
});

setTimeout(function(){
    $('.modal-close').click(function(){
        $('.modal').toggleClass('is-active');
    });
},500);
$('.addtocart').each(function(){
    var myself =$(this);
    $(this).click(function(){
        document.getElementById('alrt').innerHTML='<b>Item added to your cart!!!</b>';
        $('#alrt').css('padding','1.5rem');
        setTimeout(function() {
            document.getElementById('alrt').innerHTML='';
            $('#alrt').css('padding','unset');
        },2000);
        var itemid = myself.parents('.card').find('.myid')[0].innerHTML;

        axios.post('addtocart', {
              item_id: itemid
          })
          .then(function (response) {
            $('.cartitemscount').empty();
            $('.cartitemscount')[0].innerHTML = response['data'];
          })
          .catch(function (error) {
            console.log(error);
          })
          .then(function () {
            // always executed
          });
    })
});
$('#contactUs').click(function(){
    var data = $('#contactform').serialize();
    axios.post('contactform', data)
      .then(function (response) {
        $('.forminput').each(function(){
            $(this).val('');
        });
        $('input').change(function(){
            if(!($(this).val().length ===0)){
                if ($(this).parent('.control').find('.icon.is-right').length) {
                }else{
                    $(this).parent('.control').append(`<span class='icon is-right'><i class='fa fa-check' style='color:#00d1b2'></i></span>`)
                }
            }else{
                $(this).parent('.control').find('.icon.is-right').remove();
            }
        });

      })
      .catch(function (error) {
        alert(error);
      })
      .then(function () {
        // always executed
      });
});
$(function(){
    $('input').change(function(){
        if(!($(this).val().length ===0)){
            if ($(this).parent('.control').find('.icon.is-right').length) {
            }else{
                $(this).parent('.control').append(`<span class='icon is-right'><i class='fa fa-check' style='color:#00d1b2'></i></span>`)
            }
        }else{
            $(this).parent('.control').find('.icon.is-right').remove();
        }
    });
    $('.submit').click(function(){
        $('.form').submit();
    });
    var total= 0.00;
    $('.price').each(function(){
        total += parseFloat($(this)[0].innerHTML);
    });
    $('.total-vat')[0].innerHTML = "R" + (total*0.15).formatMoney(2);
    $('.total')[0].innerHTML = "R" + total.formatMoney(2);






});
