<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;
use App\Item;
use App\User;
use App\Transaction;
use App\TransactionDetail;
use Carbon\Carbon;
use PDF;
use App\Mail\OrdersNeedApproval;
use App\Mail\OrdersToShip;
use Illuminate\Support\Facades\Mail;

class CartController extends Controller
{

    public function __construct()
{

    // Alternativly
    $this->middleware('auth', ['except' => ['aboutus','landing','store', 'onlinestore']]);
}
    public function aboutus(){
        if (auth()->check()) {
            $cartitems = Cart::where('user_id',auth()->user()->id)->count();
        }else{
            $cartitems = Cart::count();
        }
        return view('pages.aboutus.index', compact('cartitems'));

    }

    public function landing(){
        $minid = Item::select('id')->min('id');
        $maxid = Item::select('id')->max('id');
        $ids = array();
        for ($i=0; $i <= 11 ; $i++) {
            array_push($ids,rand($minid, $maxid));
        }
        $items = Item::whereIn('id', $ids)->get();
        if (auth()->check()) {
            $cartitems = Cart::where('user_id',auth()->user()->id)->count();
        }else{
            $cartitems = Cart::count();
        }

        return view('pages.landing.index', compact('items','cartitems'));
    }
    public function onlinestore(Request $request){
        if (isset($request->queryval)) {
            $queryval = $request->queryval;
            if($queryval == "all"){
                $items = Item::all();
            }elseif($queryval == 1){
                $items = Item::where('description', 'Pre-Workout')->get();
            }elseif($queryval == 2){
                $items = Item::where('description', 'Whey Protein')->get();
            }elseif($queryval == 3){
                $items = Item::where('description', 'Meal Replacements')->get();
            }

        }else{
            $minid = Item::select('id')->min('id');
            $maxid = Item::select('id')->max('id');
            $ids = array();
            for ($i=0; $i <= 11 ; $i++) {
                array_push($ids,rand($minid, $maxid));

            }
            $items = Item::whereIn('id', $ids)->get();
        }

        if (auth()->check()) {
            $cartitems = Cart::where('user_id',auth()->user()->id)->count();
        }else{
            $cartitems = Cart::count();
        }
        return view('pages.store.index', compact('items','cartitems'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartitemsall = Cart::where('user_id',auth()->user()->id)->get();
        $cartitems = Cart::where('user_id',auth()->user()->id)->count();

        return view('pages.cart.index', compact('cartitems', 'cartitemsall'));
    }

    public function step2()
    {
        $cartitemsall = Cart::where('user_id',auth()->user()->id)->get();
        $cartitems = Cart::where('user_id',auth()->user()->id)->count();
        $userdetails = User::where('id', auth()->user()->id)->first();

        return view('pages.cart.step2', compact('cartitems', 'cartitemsall', 'userdetails'));
    }
    public function step3(Request $request)
    {
        $cartitemsall = Cart::where('user_id',auth()->user()->id)->get();
        $cartitems = Cart::where('user_id',auth()->user()->id)->count();
        $userdetails = User::where('id', auth()->user()->id)->first();
        $userdetails->address = $request->address;
        $userdetails->postalcode = $request->postalcode;
        $userdetails->phone_cell = $request->cell;
        $userdetails->save();

        return view('pages.cart.step3', compact('cartitems', 'cartitemsall', 'userdetails'));
    }
    public function step4()
    {
        $cartitemsall = Cart::where('user_id',auth()->user()->id)->get();
        $total = 0;
        foreach ($cartitemsall as $key => $cartitem) {
            $total += floatval($cartitem->items->first()->cost_incl * $cartitem->quantity);
        }
        $invnumber =  Transaction::orderBy('id', 'desc')->first()->invnumber;
        $newinvnumber = "INV" .str_pad(intval(substr(trim($invnumber),4))+1,6,'0', STR_PAD_LEFT);
        $transaction = new Transaction;
        $transaction->invnumber = $newinvnumber;
        $transaction->invdate = Carbon::now();
        $transaction->user_id = auth()->user()->id;
        $transaction->invtotal = $total;
        $transaction->save();
        foreach ($cartitemsall as $key => $cartitem) {
            $detail = new TransactionDetail;
            $detail->transaction_id = $transaction->id;
            $detail->item_id = $cartitem->items->first()->id;
            $detail->qty = $cartitem->quantity;
            $detail->approved = 0;
            $detail->save();
            $cartitem->delete();
        }

        $userdetails = User::where('id', auth()->user()->id)->first();

        $cartitems = Cart::where('user_id',auth()->user()->id)->count();
        //return view('pages.cart.invoice', compact('cartitems', 'cartitemsall', 'userdetails', 'transaction'));
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('pages.cart.order', compact('cartitems', 'cartitemsall', 'userdetails', 'transaction'));

        Mail::to("byron@treeline.co.za")->send(new OrdersNeedApproval($transaction));
        $pdf->save(storage_path('app/orders/'.$transaction->id.Date('Y-m-d').'.pdf'));
        return view('pages.cart.step4', compact('cartitems', 'cartitemsall', 'userdetails', 'transaction'));
        //return redirect(route('landing'));
    }

    public function printorder($id)
    {
        //dd(auth()->user()->id);
        $cartitemsall = Cart::where('user_id',auth()->user()->id)->get();
        $cartitems = Cart::where('user_id',auth()->user()->id)->count();
        $userdetails = User::where('id', auth()->user()->id)->first();

        $transaction = Transaction::where('id', $id)->first();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('pages.cart.order', compact('cartitems', 'cartitemsall', 'userdetails', 'transaction'));
        return $pdf->download(storage_path('app/orders/'.$transaction->id.Date('Y-m-d').'.pdf'));
        return redirect()->route('profile.index');
        //return $pdf->save(asset('storage/'..'.txt'))->stream();
    }
    public function printinvoice($id)
    {
        $transaction = Transaction::where('id', $id)->first();
        //$userdetail = User::where('id', $transaction->user_id)->first();
        $cartitemsall = Cart::where('user_id',$transaction->user_id)->get();
        $cartitems = Cart::where('user_id',$transaction->user_id)->count();
        $userdetails = User::where('id', $transaction->user_id)->first();

        $transaction = Transaction::where('id', $id)->first();
        $transaction->invoicepath = storage_path('app/invoices/'.$transaction->id.Date('Y-m-d').'.pdf');
        $transaction->save();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('pages.cart.invoice', compact('cartitems', 'cartitemsall', 'userdetails', 'transaction'));
        $pdf->save(storage_path('app/invoices/'.$transaction->id.Date('Y-m-d').'.pdf'));
        Mail::to("byron@treeline.co.za")->send(new OrdersToShip($transaction));
        return redirect()->route('transactions.index');
        //return $pdf->save(asset('storage/'..'.txt'))->stream();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $quantity = Cart::where('item_id', $request->item_id)->where('user_id', auth()->user()->id)->get();
        //dd($quantity);
        if (!$quantity->isEmpty()) {
            $cartquantity = $quantity->first()->quantity + 1;
        }else{
            $cartquantity = 1;
        }
        //return $request->item_id;
        $cart = Cart::updateOrCreate([
            'item_id' => $request->item_id,
            'user_id' => auth()->user()->id
        ],[
            'quantity' => $cartquantity
        ]);
        return Cart::where('user_id',auth()->user()->id)->count();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cartitem = Cart::where('id',$id)->first();
        //dd($cartitem->items);
        $cartitems = Cart::where('user_id',auth()->user()->id)->count();

        return view('pages.cart.edit', compact('cartitems', 'cartitem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cartitem = Cart::where('id',$id)->first();
        $cartitem->quantity = $request->quantity;
        $cartitem->save();

        return redirect(route('cart.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart = Cart::where('id',$id)->first();
        $cart->delete();
        return back();
    }
    public function downloadinvoiceuser($id){
        //dump(auth()->user()->id);
        //dump($id);
        $transaction = Transaction::where('user_id',auth()->user()->id)->where('id', $id)->first();
        //dd($transaction->invoicepath);
        return response()->download($transaction->invoicepath);


    }
}
