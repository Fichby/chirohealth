<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cart;
use App\Transaction;
use App\User;

class ProfileController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (auth()->check()) {

        $cartitems = Cart::where('user_id',auth()->user()->id)->count();
        $invoices = Transaction::where('user_id', auth()->user()->id)->where('invtotal','<>','0')->whereNotNull('invoicepath')->orderBy('id','desc')->get();
        $invoicecount = Transaction::where('user_id', auth()->user()->id)->where('invtotal','<>','0')->whereNotNull('invoicepath')->count();
        $image_path = array();
        $image_path = explode("/",auth()->user()->image_path);
        $path = end($image_path);
        return view('pages.profile.index', compact('user','cartitems','invoices', 'invoicecount','path'));
        // }else{
        //     $cartitems = Cart::count();
        //     return view('pages.profile.index', compact('cartitems'));
        // }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    //    if (auth()->check()) {
            $cartitems = Cart::where('user_id',auth()->user()->id)->count();
            $invoicecount = Transaction::where('user_id', auth()->user()->id)->where('invtotal','<>','0')->whereNotNull('invoicepath')->count();
            $user = User::where('id',$id)->first();
            $image_path = array();
            $image_path = explode("/",$user->image_path);
            $path = end($image_path);
            return view('pages.profile.edit', compact('user','cartitems', 'invoicecount','path'));
        // }else{
        //     $cartitems = Cart::count();
        //     return view('pages.profile.edit', compact('cartitems'));
        // }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id); //Get role specified by id
        //dd($user);
    //Validate name, email and password fields
        $this->validate($request, [
            'firstname'=>'required|max:120',
            'email'=>'required',
            'client_id' => 'required'

        ]);
        $input = $request->only(['firstname', 'lastname', 'client_id','address','postalcode','phone_home','phone_work','phone_cell','email']); //Retreive the name, email and password fields
        //dd($input);
        $user->fill($input)->save();


        return back();
    }
    public function profilepicture(Request $request){
        if ($request->hasFile('import_file')) {
            $path = $request->file('import_file')->store('public/profile_pictures');
            $user = User::where('id',auth()->user()->id)->first();
            //dd($user);
            $user->image_path = $path;
            $user->save();
            return back();
        }else{
            return back()->with('flash_message',
             'Please select a file!.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
