<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PatientVisits extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $month = '';
        $year = '';
        $patients = DB::select(DB::raw('SELECT month(created_at) as month,year(created_at) as year, count(id) as cnt FROM chirohealth.appointments

        group by month(created_at),year(created_at)

        '));
        return view('reports.patientvisits.index', compact('patients', 'month', 'year'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
        if (isset($request->month) && isset($request->year)) {
            $where = "where month(created_at) = $request->month and year(created_at) = $request->year";
        }elseif(isset($request->month) && !isset($request->year)){
            $where = "where month(created_at) = $request->month and year(created_at) = year(CURDATE())";
        }elseif(!isset($request->month) && isset($request->year)){
            $where = "where month(created_at) = month(CURDATE()) and year(created_at) = $request->year";
        }else{
            $where = "";
        }

        $patients = DB::select(DB::raw("SELECT month(created_at) as month, year(created_at) as year, count(id) as cnt FROM chirohealth.appointments
        $where
        group by month(created_at),year(created_at)
        "));

        return view('reports.patientvisits.index', compact('patients', 'month', 'year'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
