<?php

namespace App\Http\Controllers;

use App\Item;
use DB;
use Excel;
use Illuminate\Http\Request;
use App\User;
use App\Reference;
use App\Transaction;
use App\TransactionDetail;

class MaatwebsiteController extends Controller
{

    public function __construct(){
        $this->middleware('isAdmin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import()
    {
        return view('import.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importExcel(Request $request)
    {
        $request->validate([
            'import_file' => 'required'
        ]);

        $path = $request->file('import_file')->getRealPath();
        $data = Excel::load($path)->get();

        foreach ($data as $sheet) {
            $rownumber = 1;
            if ($sheet->getTitle() == "ClientData") {
                foreach ($sheet as $row) {
                    //sdump($row);
                    $reference_id  = Reference::where('description', 'LIKE','%'.$row->c_reference.'%')->first();
                    if(empty($row->c_email)){
                        return back()->with('failed', 'Please make sure each customer has an email address. Stopped at row: '.$rownumber );
                    };

                    $user = User::updateOrCreate([
                        'email' => $row->c_email
                    ],[
                        'client_id' => $row->client_id,
                        'firstname' => $row->c_name,
                        'lastname' => $row->c_surname,
                        'address' => $row->address,
                        'postalcode' => $row->code,
                        'phone_home' => $row->c_tel_h,
                        'phonr_work' => $row->c_tel_w,
                        'phone_cell' => $row->c_tel_cell,
                        'reference_id' => $reference_id,
                    ]);
                    //dump($user);
                    $rownumber +=1;
                }
            }elseif($sheet->getTitle() == "Invoice_info"){

                foreach ($sheet as $row) {
                    //dd($row);
                    $user  = User::where('client_id', $row->client_id )->first();
                    if(!$user){
                        dd($row);
                        return back()->with('failed', 'This transaction does not link to an existing customer. Stopped at row: '.$rownumber );
                    };

                    $transaction = Transaction::updateOrCreate([
                        'invnumber' => $row->invnum
                    ],[
                        'invdate' => $row->inv_date,
                        'user_id' => $user->id,
                        'invtotal' => $row->total,
                    ]);

                    $item = Item::where('suppl_id', 'consultation')->first();
                    if(empty($row->consultation)){
                    }else{
                        $transactiondetail = TransactionDetail::updateOrCreate([
                            'transaction_id' => $transaction->id,
                            'item_id' => $item->id
                        ],[
                            'qty' => 1
                        ]);
                    }
                    if (!is_null($row->suppl1)) {
                        //dump($rownumber);
                        //dump($row->suppl1);
                        $item = Item::where('suppl_id', $row->suppl1)->first();

                        if(!$item){

                            //dd($row->suppl1);
                            return back()->with('failed', 'This item does not exist in our dataBase: '.$row->suppl1.' please make sure it exists in the supplements sheet and make sure to move the supplements sheet before the invoice_info sheet. Stopped at row1: '.$rownumber );
                        }

                        $transactiondetail = TransactionDetail::updateOrCreate([
                            'transaction_id' => $transaction->id,
                            'item_id' => $item->id
                        ],[
                            'qty' => $row->quantity1
                        ]);
                    }
                    if (!is_null($row->suppl2)) {
                        $item = Item::where('suppl_id', $row->suppl2)->first();
                        if(!$item){
                            return back()->with('failed', 'This item does not exist in our dataBase: '.$row->suppl2.' please make sure it exists in the supplements sheet and make sure to move the supplements sheet before the invoice_info sheet. Stopped at row2: '.$rownumber );
                        }

                        $transactiondetail = TransactionDetail::updateOrCreate([
                            'transaction_id' => $transaction->id,
                            'item_id' => $item->id
                        ],[
                            'qty' => $row->quantity2
                        ]);
                    }
                    if (!is_null($row->suppl3)) {
                        $item = Item::where('suppl_id', $row->suppl3)->first();
                        if(!$item){
                            return back()->with('failed', 'This item does not exist in our dataBase: '.$row->suppl3.' please make sure it exists in the supplements sheet and make sure to move the supplements sheet before the invoice_info sheet. Stopped at row3: '.$rownumber );
                        }

                        $transactiondetail = TransactionDetail::updateOrCreate([
                            'transaction_id' => $transaction->id,
                            'item_id' => $item->id
                        ],[
                            'qty' => $row->quantity3
                        ]);
                    }
                    if (!is_null($row->suppl4)) {
                        $item = Item::where('suppl_id', $row->suppl4)->first();
                        if(!$item){
                            return back()->with('failed', 'This item does not exist in our dataBase: '.$row->suppl4.' please make sure it exists in the supplements sheet and make sure to move the supplements sheet before the invoice_info sheet. Stopped at row4: '.$rownumber );
                        }

                        $transactiondetail = TransactionDetail::updateOrCreate([
                            'transaction_id' => $transaction->id,
                            'item_id' => $item->id
                        ],[
                            'qty' => $row->quantity4
                        ]);
                    }
                    if (!is_null($row->suppl5)) {
                        $item = Item::where('suppl_id', $row->suppl5)->first();
                        if(!$item){
                            return back()->with('failed', 'This item does not exist in our dataBase: '.$row->suppl5.' please make sure it exists in the supplements sheet and make sure to move the supplements sheet before the invoice_info sheet. Stopped at row5: '.$rownumber );
                        }

                        $transactiondetail = TransactionDetail::updateOrCreate([
                            'transaction_id' => $transaction->id,
                            'item_id' => $item->id
                        ],[
                            'qty' => $row->quantity5
                        ]);
                    }
                    if (!is_null($row->suppl6)) {
                        $item = Item::where('suppl_id', $row->suppl6)->first();
                        if(!$item){
                            return back()->with('failed', 'This item does not exist in our dataBase: '.$row->suppl6.' please make sure it exists in the supplements sheet and make sure to move the supplements sheet before the invoice_info sheet. Stopped at row6: '.$rownumber );
                        }

                        $transactiondetail = TransactionDetail::updateOrCreate([
                            'transaction_id' => $transaction->id,
                            'item_id' => $item->id
                        ],[
                            'qty' => $row->quantity6
                        ]);
                    }
                    if (!is_null($row->suppl7)) {
                        $item = Item::where('suppl_id', $row->suppl7)->first();
                        if(!$item){
                            return back()->with('failed', 'This item does not exist in our dataBase: '.$row->suppl7.' please make sure it exists in the supplements sheet and make sure to move the supplements sheet before the invoice_info sheet. Stopped at row7: '.$rownumber );
                        }

                        $transactiondetail = TransactionDetail::updateOrCreate([
                            'transaction_id' => $transaction->id,
                            'item_id' => $item->id
                        ],[
                            'qty' => $row->quantity7
                        ]);
                    }

                    $rownumber +=1;
                }
            }
            //dd($sheet);
        }
        // if($data->count()){
        //     foreach ($data as $key => $value) {
        //         $arr[] = ['title' => $value->title, 'description' => $value->description];
        //     }
        //
        //     if(!empty($arr)){
        //         Item::insert($arr);
        //     }
        // }

        return back()->with('success', 'Insert Record successfully.');
    }
}
