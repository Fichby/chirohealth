<?php

namespace App\Http\Controllers;

use App\TransactionToApprove;
use App\TransactionDetail;
use Illuminate\Http\Request;
use DB;
use App\Item;
use App\Mail\OrdersToShip;
use Illuminate\Support\Facades\Mail;
use App\Transaction;
use App\Shipments;
use App\Mail\OrdersToInvoice;

class TransactionToApproveController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = DB::select(DB::raw('SELECT transactions.id as tran_id, transaction_details.id as detail_id, transactions.invnumber, items.id as item_id, items.suppl_id, items.description, transaction_details.qty
FROM
chirohealth.items left outer join
chirohealth.transaction_details on items.id = transaction_details.item_id left outer join
chirohealth.transactions on transactions.id = transaction_details.transaction_id
where transaction_details.approved = 0;'));
return view('transactionsToApprove.index', compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $tran_id = $request->tran_id;

        $detail = TransactionDetail::where('transaction_id',$tran_id )->update(['approved' => 1 ]);

        $transaction = Transaction::where('id', $tran_id)->first();

        //dd($detail)

        $shipment = new Shipments;
        $shipment->transaction_id = $transaction->id;
        $shipment->save();
        Mail::to("byron@treeline.co.za")->send(new OrdersToInvoice($transaction));
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TransactionToApprove  $transactionToApprove
     * @return \Illuminate\Http\Response
     */
    public function show(TransactionToApprove $transactionToApprove)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TransactionToApprove  $transactionToApprove
     * @return \Illuminate\Http\Response
     */
    public function edit(TransactionToApprove $transactionToApprove)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TransactionToApprove  $transactionToApprove
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TransactionToApprove $transactionToApprove)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TransactionToApprove  $transactionToApprove
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransactionToApprove $transactionToApprove)
    {
        //
    }
}
