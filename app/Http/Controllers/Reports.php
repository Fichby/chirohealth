<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Reports extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reports.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function patientsfortheday(){
        $patients = DB::select(DB::raw('select firstname, lastname,appointment_date, duration, comment
        from chirohealth.appointments inner join chirohealth.customers on appointments.customer_id = customers.id
        where date(appointments.appointment_date) = date(current_date())
        '));
        return view('reports.patientsfortheday.index', compact('patients'));
    }
    public function upcomingpatientbirthdays(){
        $patients = DB::select(DB::raw("select firstname, lastname,email,
            str_to_date(concat(year(current_date()),'-',substring(client_id,3,2),'-',substring(client_id,5,2)),'%Y-%m-%d') as birthdate
            from  chirohealth.customers
            where substring(client_id,3,2) >= month(current_date()) and substring(client_id,3,2) <= month(current_date())+1 and
            str_to_date(concat(year(current_date()),'-',substring(client_id,3,2),'-',substring(client_id,5,2)),'%Y-%m-%d') > current_date()
            order by birthdate
        "));
        return view('reports.upcomingpatientbirthdays.index', compact('patients'));
    }
    public function reorderlevels(){
        $items = DB::select(DB::raw("SELECT suppl_id,items.description, min_levels, qtyonhand, vendors.description as vendordesc, vendors.phone_work, vendors.email FROM chirohealth.items
            left outer join chirohealth.vendors on items.vendor_id = vendors.id
            where qtyonhand < min_levels
        "));
        return view('reports.reorderlevels.index', compact('items'));
    }


}
