<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactUs;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function contactform(Request $request){
        $name = $request->name;
        $email = $request->email;
        $comment = $request->comment;
        Mail::to("byron@treeline.co.za")->send(new ContactUs($name, $email, $comment));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


}
