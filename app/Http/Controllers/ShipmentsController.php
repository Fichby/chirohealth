<?php

namespace App\Http\Controllers;

use App\TransactionToApprove;
use App\TransactionDetail;
use Illuminate\Http\Request;
use App\Shipments;
use App\Item;
use App\Transaction;

class ShipmentsController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shipments = Shipments::all();
        return view('shipments.index', compact('shipments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TransactionToApprove  $transactionToApprove
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::where('id' , $id)->first();
        $transactiondetails = $transaction->details;
        return view('shipments.show', compact('transaction','transactiondetails'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TransactionToApprove  $transactionToApprove
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $transaction = Transaction::where('id', $id)->first();
        foreach ($transaction->details as $value) {
            $item = Item::where('id', $value->item_id)->first();
            $item->qtyonhand = $item->qtyonhand -$value->qty;
            $item->save();
        }
        Shipments::where('transaction_id', $id)->delete();
        return redirect()->route('shipments.index');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TransactionToApprove  $transactionToApprove
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TransactionToApprove $transactionToApprove)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TransactionToApprove  $transactionToApprove
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransactionToApprove $transactionToApprove)
    {
        //
    }
}
