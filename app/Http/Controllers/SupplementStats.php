<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SupplementStats extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $month = '';
        $year = '';
        $supplements = DB::select(DB::raw('SELECT month(transactions.created_at) as month, year(transactions.created_at) as year, items.suppl_id, items.description, count(transactions.id) as cnt
FROM chirohealth.transactions left outer join chirohealth.transaction_details on transactions.id = transaction_details.transaction_id
inner join chirohealth.items on transaction_details.item_id = items.id

group by month(transactions.created_at), year(transactions.created_at),items.suppl_id, items.description
        '));
        return view('reports.supplementstats.index', compact('supplements','month','year'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
        if (isset($request->month) && isset($request->year)) {
            $where = "where month(transactions.created_at) = $request->month and year(transactions.created_at) = $request->year";
        }elseif(isset($request->month) && !isset($request->year)){
            $where = "where month(transactions.created_at) = $request->month and year(transactions.created_at) = year(CURDATE())";
        }elseif(!isset($request->month) && isset($request->year)){
            $where = "where month(transactions.created_at) = month(CURDATE()) and year(transactions.created_at) = $request->year";
        }else{
            $where = "";
        }

        $supplements = DB::select(DB::raw("SELECT month(transactions.created_at) as month, year(transactions.created_at) as year, items.suppl_id, items.description, count(transactions.id) as cnt
FROM chirohealth.transactions left outer join chirohealth.transaction_details on transactions.id = transaction_details.transaction_id
inner join chirohealth.items on transaction_details.item_id = items.id
        $where
group by month(transactions.created_at), year(transactions.created_at),items.suppl_id, items.description
        "));

        return view('reports.patientvisits.index', compact('supplements', 'month', 'year'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
