<?php

namespace App\Http\Controllers;

use App\CustomerNote;
use Illuminate\Http\Request;

class CustomerNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerNote  $customerNote
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerNote $customerNote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerNote  $customerNote
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerNote $customerNote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerNote  $customerNote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerNote $customerNote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerNote  $customerNote
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerNote $customerNote)
    {
        //
    }
}
