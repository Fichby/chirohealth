<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use DB;

class ItemController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = DB::select(DB::raw('select items.id, items.suppl_id, items.description, items.min_levels, items.cost_excl, items.cost_incl, items.perc_inc, items.vendor_id, items.qtyonhand, a.qty as qtyonsalesorder,items.created_at, items.updated_at
from chirohealth.items left outer join (
SELECT items.id as id, items.suppl_id as suppl_id, items.description, transaction_details.qty
FROM
chirohealth.items left outer join
chirohealth.transaction_details on items.id = transaction_details.item_id left outer join
chirohealth.transactions on transactions.id = transaction_details.transaction_id
where transaction_details.approved = 0) a on items.id = a.id;'));

        return view('items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'suppl_id'=>'required|unique:items',
            'cost_excl'=>'required',
            'cost_incl'=>'required',
            'min_levels' =>'required'
        ]);
        $item = new Item;
        $item->description = $request->description;
        $item->suppl_id = $request->suppl_id;
        $item->cost_excl = $request->cost_excl;
        $item->cost_incl = $request->cost_incl;
        $item->perc_inc = $request->perc_inc;
        $item->min_levels = $request->min_levels;
        $item->save();
        return redirect()->route('items.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::where('id', $id)->first();
        return view('items.edit', compact("item"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'suppl_id'=>'required',
            'cost_excl'=>'required',
            'cost_incl'=>'required',
            'min_levels' =>'required'
        ]);
        $item = Item::where('id', $id)->first();
        $item->description = $request->description;
        $item->suppl_id = $request->suppl_id;
        $item->cost_excl = $request->cost_excl;
        $item->cost_incl = $request->cost_incl;
        $item->perc_inc = $request->perc_inc;
        $item->min_levels = $request->min_levels;
        $item->save();
        return redirect()->route('items.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::where('id', $id)->first();
        $item->delete();
        return back();
    }
}
