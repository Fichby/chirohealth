<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ClearanceMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (Auth::gaurd('admin')->user()->hasPermissionTo('Administer roles & permissions')) //If user has this //permission
    {
            return $next($request);
        }

        if ($request->is('admins/create'))//If user is creating a post
         {
            if (!Auth::gaurd('admin')->user()->hasPermissionTo('Administer roles & permissions'))
         {
                abort('401');
            }
         else {
                return $next($request);
            }
        }

        if ($request->is('posts/*/edit')) //If user is editing a post
         {
            if (!Auth::gaurd('admin')->user()->hasPermissionTo('Edit Post')) {
                abort('401');
            } else {
                return $next($request);
            }
        }

        if ($request->isMethod('Delete')) //If user is deleting a post
         {
            if (!Auth::gaurd('admin')->user()->hasPermissionTo('Delete Post')) {
                abort('401');
            }
         else
         {
                return $next($request);
            }
        }

        return $next($request);
    }
}
