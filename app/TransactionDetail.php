<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $guarded = [];
    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }
    public function items(){
        return $this->hasMany('App\Item', 'id', 'item_id');
    }
}
