<?php

namespace App\Mail;
use App\Transaction;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrdersNeedApproval extends Mailable
{
    use Queueable, SerializesModels;
    public $transaction;
    /**
     * Create a new message instance.
     *
     * @return void
     */
     public function __construct(Transaction $transaction)
     {
         $this->transaction = $transaction;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('byronfich@gmail.com')->markdown('emails.orders.approvals');
    }
}
