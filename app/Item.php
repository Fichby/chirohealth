<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = [];

    public function cart(){
        return $this->belongsTo('App\Cart');
    }
}
