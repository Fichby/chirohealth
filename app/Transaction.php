<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = [];
    public function details()
    {
        return $this->hasMany('App\TransactionDetail');
    }
    public function customer()
    {
        return $this->belongsTo('App\User', "user_id", 'id');
    }
}
