<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = [
        'user_id', 'item_id', 'quantity',
    ];

    public function items(){
        return $this->hasMany('App\Item', 'id', 'item_id');
    }
}
